package com.zeslab.attendance;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ClearCacheRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.Permission;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import static java.lang.Thread.sleep;

public class MyMaps extends FragmentActivity
        implements OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener {
    Toolbar toolbar;
    String actiontoperform;
    Marker marker;
    ImageView adsimage;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    private static final String userid = "userid";
    public static final String Userid = userid;
    public static final String Username = "username";
    public static final String Officerange = "officerange";
    public static final String Loginornot = "loginornot";
    public static final String IsLogin = "islogin";
    public static final String Officelat = "Officelat";
    public static final String Officelong = "Officelong";
    public static final String Userlat = "userlat";
    public static final String Userlong = "userlong";
    public static final String Actiontoperform = "actiontoperform";
    public static final String Settingattendancebuttonvalue = "settingattendancebuttonvalue";
    public static final String Settingfirsttimeattendancevalue = "settingfirsttimeattendancevalue";
    public static final String Officebtnvorg = "setofficebtn";
    public static final String Automatic = "auto";
    public static final String Automatictext = "autotext";
    public static final String Groupname = "groupname";
    public static final String Currentdate = "currentdate";
    public static final String Managername = "managername";
    public static final String Intro = "intro";

    public static final String TAG1 = MyMaps.class.getName();


    private Context context;
    private Activity activity;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private GoogleMap mMap;
    LocationManager locationManager;
    LocationListener locationListener;
    //    Location location;
    String provider;
    String in_Date;
    Double lat, lan;
    Button in, out;
    int mYear, mMonth, mDate, out_Date, in_Hours, in_Minutes, out_Minutes, out_Hours;
    JSONObject jo, ji;
    ImageView rightMenu;
    Button in_button, auto;
    utlitis util;
    TextView set_Location;
    static final String Atcid = "atcid";
    String Id, getId, getUserId;
    String ATW;
    String respn;
    LatLng user_location, lastlocation;
    NavigationView navigationView;
    DrawerLayout drawer;
    Calendar calendar;
    SimpleDateFormat format;
    TextView userNameTitle,distance;
    double latitude = 0.0, longitude = 0.0;
    LocationTracker locationTracker;
    Bitmap ads_image;
    SupportMapFragment mapFragment;
    // flag for one time location update
    boolean isLocationUpdated = true;

//    LocationManager mLocationManager;
//   public Context mcontext;
//    Location myLocation = getLastKnownLocation(mcontext);

//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case MY_PERMISSIONS_READ_PHONE_STATE: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    // permission was granted, yay! Do the
//                    // read-phone-state-related task you need to do.
//
//
//                } else {
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
//                }
//                return;
//            }
//
//            // other 'case' lines to check for other
//            // permissions this app might request
//        }
//    }


//    public void insert_attendence_into_database() {
//        RequestQueue reqC = Volley.newRequestQueue(this);
//        String ip_port = getString(R.string.url);
//        String url = ip_port + "/attendence";
//        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
//                Context.MODE_PRIVATE);
//        in_button.setText("Working...");
//        JsonObjectRequest str_req = new JsonObjectRequest(Request.Method.POST,
//                url, jo, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//
//                /////////RESPONSE JSON STORE START
//                respn = response.toString();
//                Log.i("user location server", String.valueOf(respn));
//                savingId();
//
//            }
//        },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Log.i("volley error", error.toString());
//                        in_button.setText(sharedpreferences.getString(Settingattendancebuttonvalue, ""));
//                        in_button.setEnabled(true);
//                        Toast.makeText(getApplicationContext(), "operation failed", Toast.LENGTH_LONG).show();
//                    }
//
//                });
//
//        // add json request in volley request queue
//        reqC.add(str_req);
//
//        //Toast.makeText(getApplicationContext(), "Thank You !!", Toast.LENGTH_LONG).show();
//
//    }

//    public void savingId() {
//        try {
//            ji = new JSONObject(respn);
//            Id = ji.getString("_id");
//            ATW = ji.getString("action_to_perform");
//            sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
//                    Context.MODE_PRIVATE);
//            if (ATW.equals("InTime")) {
//                sharedpreferences.edit().putString(Actiontoperform, "OUT").apply();
//                String atf = sharedpreferences.getString(Actiontoperform, "");
//                Log.i("atf_out", atf);
//                sharedpreferences.edit().putString(Settingattendancebuttonvalue, "OUT").apply();
//                in_button.setText(sharedpreferences.getString(Settingattendancebuttonvalue, ""));
//                in_button.setEnabled(true);
//            } else if (ATW.equals("OutTime")) {
//                sharedpreferences.edit().putString(Actiontoperform, "IN").apply();
//                String atf = sharedpreferences.getString(Actiontoperform, "");
//                Log.i("atf_in", atf);
//                sharedpreferences.edit().putString(Settingattendancebuttonvalue, "IN").apply();
//                in_button.setText(sharedpreferences.getString(Settingattendancebuttonvalue, ""));
//                in_button.setEnabled(true);
//            }
//            Toast.makeText(getApplicationContext(), ATW, Toast.LENGTH_LONG).show();
//
//            SharedPreferences.Editor editor = sharedpreferences.edit();
//            editor.putString(Atcid, Id); // Storing string
//            editor.commit();
//            String show = sharedpreferences.getString(Atcid, "");
//            Log.i("show", show);
////            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            in_button.setText(sharedpreferences.getString(Settingattendancebuttonvalue, ""));
//            in_button.setEnabled(true);
//        }
//    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private boolean checkPermission1(){
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;

        }
    }

    private void requestPermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,Manifest.permission.ACCESS_FINE_LOCATION)){

            Toast.makeText(context,"GPS permission allows us to access location data. Please allow in App Settings for additional functionality.",Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_REQUEST_CODE);
        }
    }

    ///////////////   ASKING FOR STORAGE PERMISSION  /////////////////////
    public  boolean isLocationGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.i("TAG","Permission is granted");
                return true;
            } else {
                Log.i("TAG","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.i("TAG","Permission is granted");

            return true;
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_maps);
        toolbar = findViewById(R.id.toolbar);
        drawer = findViewById(R.id.maps_main);
        set_Location = findViewById(R.id.set_Location);
        utlitis.getInstance().set_Location = set_Location;
        rightMenu = findViewById(R.id.rightmenu);
        rightMenu.setVisibility(View.VISIBLE);
        auto = findViewById(R.id.auto);
        utlitis.getInstance().auto = auto;
        in_button = findViewById(R.id.in_btn);
        utlitis.getInstance().in_button = in_button;
        context = getApplicationContext();
        activity = this;
        distance = (TextView) findViewById(R.id.diatance);
        utlitis.getInstance().distance = distance;
        adsimage = (ImageView) findViewById(R.id.adsimage);
        utlitis.getInstance().maps = this;
        utlitis.getInstance().context = getApplicationContext();
        utlitis.getInstance().isLocationUpdated = isLocationUpdated;

        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        if (sharedpreferences.getBoolean(Automatic, false)) {
            in_button.setVisibility(View.GONE);
            auto.setVisibility(View.VISIBLE);
        }


        int newDate = Calendar.getInstance().get(Calendar.DATE);
        int getOldDate = sharedpreferences.getInt(Currentdate,0);
        if(getOldDate != newDate){
            sharedpreferences.edit().putString(Actiontoperform,"IN").commit();
            sharedpreferences.edit().putString(Settingattendancebuttonvalue,"IN").commit();
        }

        String firsttime = sharedpreferences.getString(Settingfirsttimeattendancevalue, "");
        if (!firsttime.equals("2")) {
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(Settingattendancebuttonvalue, "Your office location is not set");
            editor.putString(Settingfirsttimeattendancevalue, "2");
            editor.putString(Automatictext,"Automatic mode is on");
            editor.putInt(Currentdate,4);
            editor.putString(Managername,"no");
            editor.apply();
            set_Location.setVisibility(View.VISIBLE);
            in_button.setEnabled(false);
//            Toast.makeText(getApplicationContext(),"button desabled",Toast.LENGTH_LONG).show();

        }
        String visible_or_gone = sharedpreferences.getString(Officebtnvorg, "");
        if (visible_or_gone.equals("GONE")) {
            set_Location.setVisibility(View.GONE);
        }else {
            in_button.setEnabled(false);
        }
        String toast = sharedpreferences.getString(Settingattendancebuttonvalue, "");
//        Toast.makeText(getApplicationContext(),toast,Toast.LENGTH_LONG).show();


        String locale = getApplicationContext().getResources().getConfiguration().locale.getCountry();
//        String locale = getApplicationContext().getResources().getConfiguration().


        TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String countryCode = tm.getSimCountryIso();
        String timezone = TimeZone.getDefault().getID();

//        String mPhoneNumber = tm.getLine1Number();
//        Toast.makeText(getApplicationContext(),mPhoneNumber+" is my phone number",Toast.LENGTH_LONG).show();

//        Log.i("pnone number",mPhoneNumber);

//        Log.i("getting user ajent",ua);
//        Toast.makeText(getApplicationContext(),"ajent"+ua ,Toast.LENGTH_LONG).show();

//        Log.i("country code",timezone.toString());
//        Toast.makeText(getApplicationContext(),"country code"+timezone,Toast.LENGTH_LONG).show();

//        out_button = findViewById(R.id.out_btn);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            List<SubscriptionInfo> subscription = SubscriptionManager.from(getApplicationContext()).getActiveSubscriptionInfoList();
//            for (int i = 0; i < subscription.size(); i++) {
//                SubscriptionInfo info = subscription.get(i);
//                Log.d("mobile number", "number " + info.getNumber());
//                Log.d("network name", "network name : " + info.getCarrierName());
//                Log.d("country iso", "country iso " + info.getCountryIso());
//                Toast.makeText(getApplicationContext()," phone number",Toast.LENGTH_LONG).show();
//            }
//        }

        userNameTitle = (TextView) findViewById(R.id.title);
        String title = sharedpreferences.getString(Username, "");
        String groupname = sharedpreferences.getString(Groupname, "");
        int tl = title.length();
        int gl = groupname.length();
        String s = title + " : " + groupname;
        SpannableString ss1 = new SpannableString(s);
//        ss1.setSpan(new RelativeSizeSpan(1.2f), 0,tl, 0);
        ss1.setSpan(new RelativeSizeSpan(0.5f), tl + 3, tl + 3 + gl, 0);// set size
//        ss1.setSpan(new ForegroundColorSpan(Color.RED), 0, 5, 0);// set color

//        tv.setText(ss1);
        userNameTitle.setText(ss1);
//        in_button.setEnabled(false);
//        out_button.setEnabled(false);
        in_button.setText(toast);
        auto.setText(sharedpreferences.getString(Automatictext, ""));

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        ////////////////////     declearing calendar
        calendar = Calendar.getInstance();
        format = new SimpleDateFormat("dd/MM/yyyy");


        System.out.println(format.format(calendar.getTime()));
        String date1 = format.format(calendar.getTime());
        Log.i("hours", String.valueOf(Calendar.getInstance().get(Calendar.HOUR)));
        Log.i("minutes", String.valueOf(Calendar.getInstance().get(Calendar.MINUTE)));
        Log.i("date1", date1);
        Log.i("months", String.valueOf(Calendar.getInstance().get(Calendar.MONTH)));
        Log.i("year", String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));


//        locationTracker = new LocationTracker(this);
        //////////////////////////////  ******** one try
        getting_location_updates();

        //////////////////////////////  ******** one try
//        String ofbtn = sharedpreferences.getString(Officebtnvorg,"");
//        if(ofbtn.equals("GONE")){
//            set_Location.setVisibility(View.GONE);
//        }


        ////////   set location button
        set_Location.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (user_location != null) {
                    if (user_location.latitude == 0.0 && user_location.longitude == 0.0) {
                        getting_location_updates();
                        Toast.makeText(getApplicationContext(), "not getting correct " +
                                "office location", Toast.LENGTH_LONG).show();
                    } else {
                        update_your_office_location();
                        Log.i("show_map", getUserId);
                    }
                } else {
                    getting_location_updates();
                    myMapReady(mMap);
                    Toast.makeText(getApplicationContext(), "not getting correct " +
                            " location please enable your gps", Toast.LENGTH_LONG).show();
                }
            }

        });


        ////// in button
        in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                in_button.setEnabled(false);
                utlitis.getInstance().making_JSONObject();

            }
        });

        ////  right menu
        rightMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MyMaps.this, MySettings.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });


//         Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ////////////////////    starting services
        startAlert();
        ///////////////// alert msg for gps
        statusCheck();
//        request_ads_image_from_server();


        /**
         * Dexter runtime permisions
         * i have added if condition manually for check
         */
//        if (Build.VERSION.SDK_INT >= 23) {
            Dexter.withActivity(this)
                    .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            getting_location_updates();
                            myMapReady(mMap);
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).check();

//            if(utlitis.getInstance().updatedLocation==false || true){
////                myMapReady(mMap);
//                moveCamera();
//            }
    }
    public void moveCamera(){
        if(locationTracker.canGetLocation()) {
            latitude = locationTracker.getLatitude();
            longitude = locationTracker.getLongitude();
            user_location = new LatLng(latitude,longitude);
            utlitis.getInstance().user_location = user_location;
//                Log.i("LocationTracker", "LatLong: " + latitude + ", " + longitude);
//                Log.i("location Tracker 1","user"+user_location.toString());
            LatLng currentloc = user_location;
            if(mMap!=null) {
                mMap.addMarker(new MarkerOptions().position(currentloc).title(" loading").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentloc));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            }
        }
    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void getting_location_updates() {
        locationTracker = new LocationTracker(context);
        if (locationTracker.canGetLocation()) {
            utlitis.getInstance().updatedLocation = true;
            latitude = locationTracker.getLatitude();
            longitude = locationTracker.getLongitude();
            user_location = new LatLng(latitude, longitude);
            utlitis.getInstance().user_location = user_location;
            Log.i("LocationTracker", "LatLong: " + latitude + ", " + longitude);
            Log.i("location Tracker 1", "user" + user_location.toString());
        } else {
            Log.i("LocationTracker", "Unable to get location");
            utlitis.getInstance().updatedLocation = false;
        }
    }

//    public void making_JSONObject(){
//        String timezone = TimeZone.getDefault().getID();
//
//        actiontoperform = sharedpreferences.getString(Actiontoperform,"");
//
//        mYear = Calendar.getInstance().get(Calendar.YEAR);
//        mMonth = Calendar.getInstance().get(Calendar.MONTH);
//        mDate = Calendar.getInstance().get(Calendar.DATE);
//        out_Date = Calendar.getInstance().get(Calendar.DATE);
//        out_Hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
//        out_Minutes = Calendar.getInstance().get(Calendar.MINUTE);
//        in_Date = format.format(calendar.getTime());
//        in_Hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
//        in_Minutes = Calendar.getInstance().get(Calendar.MINUTE);
//        getUserId = sharedpreferences.getString(Userid, "");
//        String   getUsername =  sharedpreferences.getString(Username,"");
//        getId = sharedpreferences.getString(Atcid, "");
////                    getOfficelocation = sharedpreferences.getString(Officelocation,"");
////                    Log.i("officelocation",getOfficelocation+"is office locaion");
//
//
//        jo = new JSONObject();
//        try {
//            jo.put("id", getId);
//            jo.put("User_Id", getUserId);
//            jo.put("username", getUsername);
//            jo.put("mYear", mYear);
//            jo.put("mMonth", mMonth);
//            jo.put("mDate", mDate);
//            jo.put("in_Date", in_Date);
//            jo.put("in_Hour", in_Hours);
//            jo.put("in_Minute", in_Minutes);
//            jo.put("out_Date", out_Date);
//            jo.put("out_Hour",out_Hours);
//            jo.put("out_Minute",out_Minutes);
//            jo.put("timezone",timezone);
//            jo.put("Location_on_InTime", user_location);
//            jo.put("Location_on_OutTime",user_location);
//            jo.put("officeLat", user_location.latitude);
//            jo.put("officeLong", user_location.longitude);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        if(actiontoperform.equals("IN")) {
//            try {
//                jo.put("action_to_perform", "InTime");
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }else if(actiontoperform.equals("OUT")){
//            try {
//                jo.put("id", getId);
//                jo.put("action_to_perform", "OutTime");
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//        insert_attendence_into_database();
//        Log.i("log of jo object",jo.toString());
//
//    }
//    private String getPhone() {
//        TelephonyManager phoneMgr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
//        if (ActivityCompat.checkSelfPermission(, wantPermission) != PackageManager.PERMISSION_GRANTED) {
//            return "";
//        }
//        return phoneMgr.getLine1Number();
//    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
//        utlitis.getInstance().googleMap = googleMap;
        if(googleMap !=null){
            mMap =googleMap;
            myMapReady(mMap);
        }

//        moveCamera();
    }

    public void startAlert() {
        Intent intent = new Intent(this, Myservices.class);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(
//                this.getApplicationContext(), 234324243, intent, 0);
        //PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, 0);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                + (5 * 60 * 1000), 5 * 60 * 1000, pendingIntent);
    }

//        LatLng currentloc1 = new LatLng(locationTracker.location.getLatitude(),locationTracker.location.getLongitude());
////        int i = 1;
////        Toast.makeText(this, i+"location:"+currentloc1, Toast.LENGTH_LONG).show();
////        i++;
////        if(currentloc1!= null) {
////
////            if(mMap != null) {
////                mMap.clear();
////                mMap.addMarker(new MarkerOptions().position(currentloc1).title(" current location"));
////                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentloc1));
////                mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
////            }else{
////                Toast.makeText(this, "else map is null", Toast.LENGTH_LONG).show();
////
////            }
////        }



//    public void gettingLastLocation(LocationManager locationManager,LatLng lastlocation,GoogleMap mMap){
//        if(Build.VERSION.SDK_INT < 23){
//
//            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,locationListener);
//        }
//        else {
//            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
//
//                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
//            }else {
//                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,locationListener);
//                Location lastknowlocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//                lastlocation = new LatLng(lastknowlocation.getLatitude(),lastknowlocation.getLongitude());
//                mMap.addMarker(new MarkerOptions().position(lastlocation));
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(lastlocation));
//            }
//        }
//    }


    private boolean MyStartActivity(Intent aIntent) {
        try {
            startActivity(aIntent);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_about_us) {
//            Snackbar.make(findViewById(R.id.maps_main), "App Version 1.0", Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show();
            Intent abu = new Intent(MyMaps.this, About_us.class);
            abu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(abu);

        } else if (id == R.id.nav_rate_us) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            //Try Google play
            intent.setData(Uri.parse("market://details?id=com.zeslab.attendance"));
            if (!MyStartActivity(intent)) {
                //Market (Google play) app seems not installed, let's try to open a webbrowser
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.zeslab.attendance"));
                if (!MyStartActivity(intent)) {
                    //Well if this also fails, we have run out of options, inform the user.
                    Toast.makeText(this, "Could not open Android market, please install the market app.", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (id == R.id.nav_share) {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "https://play.google.com/store/apps/details?id=com.zeslab.attendance";
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));

//        }else if (id == R.id.nav_manager_activity) {
//            Intent imanager = new Intent(MyMaps.this, ManagerActivity.class );
//            imanager.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(imanager);

        } else if (id == R.id.nav_get_developed) {
            Intent idea = new Intent(MyMaps.this, Idea_for_development.class);
            idea.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            idea.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(idea);

//        }else if (id == R.id.nav_setMySubordinate) {
//            Intent subordinate = new Intent(MyMaps.this, AdduserAsSubordinate.class );
//            subordinate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(subordinate);
////
//        }else if (id == R.id.nav_setMyManager) {
//            Intent subordinate = new Intent(MyMaps.this, AddMyManager.class );
//            subordinate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(subordinate);

        } else if (id == R.id.nav_attendance) {
            Intent subordinate = new Intent(MyMaps.this, AttendeceDetails.class);
            subordinate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            subordinate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(subordinate);

        } else if (id == R.id.nav_contact_us) {
            Intent intentEmail = new Intent(Intent.ACTION_SEND);
            intentEmail.setType("message/rfc822");
            intentEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{"ekta@zeslab.com"});
            intentEmail.putExtra(Intent.EXTRA_SUBJECT, "");
            intentEmail.putExtra(Intent.EXTRA_TEXT, "");
            startActivity(Intent.createChooser(intentEmail, "Send Email"));
        } else if (id == R.id.nav_help) {
            sharedpreferences = getApplicationContext().getSharedPreferences(mypreference, 0); // 0 - for private mode
            sharedpreferences.edit().putString(Intro, "2").commit(); // Storing string
            Intent subordinate = new Intent(MyMaps.this, Help.class);
            subordinate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(subordinate);
        } else if (id == R.id.nav_logout) {

            Intent i = new Intent(MyMaps.this, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putBoolean(IsLogin, false);
            editor.putString(Settingfirsttimeattendancevalue, "3");
            editor.putString(Officebtnvorg, "Visible");
            editor.putBoolean(Automatic, false);
            editor.remove(Groupname);
            editor.remove(Userlat);
            editor.remove(Userid);
            editor.remove(Username);
            editor.remove(Userlong);
            editor.commit();
            LoginManager.getInstance().logOut();
            set_Location.setVisibility(View.VISIBLE);
            startActivity(i);
            finish();
        }

        return true;

    }

    @Override
    public void onBackPressed() {
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        String in_or_out = sharedpreferences.getString(Settingattendancebuttonvalue,"");
//        Toast.makeText(context,"value of in or out:"+in_or_out,Toast.LENGTH_LONG).show();
        if (drawer.isDrawerOpen(navigationView)) {
            drawer.closeDrawer(navigationView);
        } else if(in_or_out.equals("IN")){
//            adsimage.buildDrawingCache();
//            ads_image= adsimage.getDrawingCache();
//            Bundle extras = new Bundle();
//            extras.putParcelable("imagebitmap", ads_image);
            Intent i = new Intent(MyMaps.this,Idea_for_development.class);
//            i.putExtras(extras);
            startActivity(i);
            finish();
        }else {
            finish();
        }
    }

    public void update_your_office_location() {
        RequestQueue reqC = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port + "/setofficeLoc";
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        getUserId = sharedpreferences.getString(Userid, "");

        try {
            if (jo == null) {
                jo = new JSONObject();

            }
            if (user_location == null) {
                LocationTracker locationTracker1 = new LocationTracker(this);
                user_location = new LatLng(locationTracker1.getLatitude(), locationTracker1.getLongitude());
                Toast.makeText(getApplicationContext(),
                        user_location.latitude + "," + user_location.longitude, Toast.LENGTH_LONG).show();
            }
            jo.put("User_Id", getUserId);
            jo.put("officeLat", user_location.latitude);
            jo.put("officeLong", user_location.longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("uolu", user_location.toString());
        JsonObjectRequest str_req = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                /////////RESPONSE JSON STORE START
                respn = response.toString();
                Log.i("uol", String.valueOf(respn) + "respo");
                sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                int lat = (int) (user_location.latitude * 1e6);
                int log = (int) (user_location.longitude * 1e6);
                editor.putInt(Officelat, lat);
                editor.putInt(Officelong, log);
                editor.putString(Officebtnvorg, "GONE");
                editor.commit();
                in_button.setEnabled(true);
                set_Location.setVisibility(View.GONE);
                sharedpreferences.edit().putString(Settingattendancebuttonvalue, "IN").apply();
                sharedpreferences.edit().putString(Actiontoperform, "IN").apply();
                String inmsg = sharedpreferences.getString(Settingattendancebuttonvalue, "");
                in_button.setText(inmsg);
//                Toast.makeText(getApplicationContext(), inmsg, Toast.LENGTH_LONG).show();

                Toast.makeText(getApplicationContext(),
                        "your office location saved sucessfull", Toast.LENGTH_LONG).show();

//                getOfficelocation = sharedpreferences.getString(Officelat,"");
//                Log.i("uolv",getOfficelocation+" ");
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
                    }
                });
        reqC.add(str_req);
    }

//    public void isLocationinOffice(){
//        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
//                Context.MODE_PRIVATE);
//        getOfficelat = sharedpreferences.getInt(Officelat,0);
//        getOfficelong = sharedpreferences.getInt(Officelong,0);
//        getuser_lat = sharedpreferences.getInt(Userlat,0);
//        getuser_long = sharedpreferences.getInt(Userlong,0);
//        Location oLoc = new Location(provider);
//        oLoc.setLatitude(getOfficelat/1e6);
//        Log.i("userOLoc", String.valueOf(oLoc.getLatitude()));
//        oLoc.setLongitude(getOfficelong/1e6);
//
//        Log.i("userOLoc", String.valueOf(oLoc.getLongitude()));
//        Location uLoc = new Location(provider);
//        uLoc.setLatitude(user_location.latitude);
//
//        Log.i("useruLoc", String.valueOf(uLoc.getLatitude()));
//        uLoc.setLongitude(user_location.longitude);
//
//        Log.i("useruLoc", String.valueOf(uLoc.getLongitude()));
//        double displacement = uLoc.distanceTo(oLoc);
//        Log.i("displacementis", String.valueOf(displacement));
//        if(displacement <= 100){
//            Log.i("user_loc","user in the office");
//            in_button.setEnabled(true);
//        }else {
//            Log.i("user_loc","user is out of the office");
//            in_button.setEnabled(false);
//        }
//
//    }

//    private Location getLastKnownLocation(Context context) {
//        mLocationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
//        List<String> providers = mLocationManager.getProviders(true);
//        Location bestLocation = null;
//        for (String provider : providers) {
//            Location l = mLocationManager.getLastKnownLocation(provider);
//            if (l == null) {
//                continue;
//            }
//            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
//                // Found best last known location: %s", l);
//                bestLocation = l;
//                break;
//            }
//        }
//        return bestLocation;
//    }

    public void statusCheck() {

        LocationTracker locationTracker = new LocationTracker(this);
        if (!locationTracker.canGetLocation()) {
            buildAlertMessageNoGps();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void myMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (mMap != null) {
            if (googleMap != null) {
                utlitis.getInstance().googleMap = googleMap;
            }

            mMap.clear();
//            mMap.setIndoorEnabled(true);
            LatLng currentloc;
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            locationTracker = new LocationTracker(this);
            if(locationTracker.canGetLocation()) {
                latitude = locationTracker.getLatitude();
                longitude = locationTracker.getLongitude();
                user_location = new LatLng(latitude,longitude);
            user_location = new LatLng(latitude,longitude);
                utlitis.getInstance().user_location = user_location;
//                Log.i("LocationTracker", "LatLong: " + latitude + ", " + longitude);
//                Log.i("location Tracker 1","user"+user_location.toString());
                currentloc = user_location;
                mMap.addMarker(new MarkerOptions().position(currentloc).title(" Your location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentloc));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

            }
            else {
                Log.i("LocationTracker", "Unable to get location");
            }

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
                mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {
                        if(locationTracker.canGetLocation()) {
                            latitude = locationTracker.getLatitude();
                            longitude = locationTracker.getLongitude();
                            user_location = new LatLng(latitude, longitude);

                        }

                        return false;
                    }
                });

            }



        }

    }
    @Override
    protected void onResume() {
        super.onResume();
//        mapFragment.getMapAsync(this);
        myMapReady(mMap);
    }
    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        locationTracker.stopUsingGPS();
    }


    public void request_ads_image_from_server() {
//        utlitis.getInstance().progressBar.setVisibility(View.VISIBLE);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/displayoneImage";
//        simpleProgressBar.setVisibility(View.VISIBLE);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.i("ads response",response.toString());
                    boolean result = response.getBoolean("result");
                    if(result){
//                        utlitis.getInstance().progressBar.setVisibility(View.GONE);
                        String img = response.getString("Data");
                        Glide.with(context).load(img)
                                .into(adsimage);
                        adsimage.buildDrawingCache();
                        ads_image= adsimage.getDrawingCache();

                    }else{
//                        utlitis.getInstance().progressBar.setVisibility(View.GONE);
                        Toast.makeText(context,"data not " +
                                "found",Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
//                    utlitis.getInstance().progressBar.setVisibility(View.GONE);
                    Toast.makeText(context,"Exceptions",Toast.LENGTH_LONG).show();

//                            simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        utlitis.getInstance().progressBar.setVisibility(View.GONE);
                        if (error != null) {
//                            Log.d(TAG, error.toString());

                            Toast.makeText(context, "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
//
                        }
                    }
                }
        );
        requestQueue.add(jsonObjReq);
    }
}
