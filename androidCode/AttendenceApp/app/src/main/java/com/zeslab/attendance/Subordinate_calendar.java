package com.zeslab.attendance;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashSet;

public class Subordinate_calendar extends AppCompatActivity {

    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Userid = "userid";
    public static final String Username = "username";
    public static final String WhichMonth = "whichmonth";
    public static final String Usernameofsubordinate = "usernameofsubordinate";
    String getUsername;
    String getMonth;
    JSONObject jo;
    JSONArray jsonArray;
    Calendar calendar;
    private CalendarDay date;

    HashSet<CalendarDay> dates;
    MaterialCalendarView materialCalendarView;
    EventDecorator eventDecorator;
    int m;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subordinate_calendar);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        getUsername = sharedpreferences.getString(Usernameofsubordinate,"");
        getMonth = sharedpreferences.getString(WhichMonth,"");
        Log.i("value of month",getMonth);
        m = Integer.parseInt(getMonth);
        materialCalendarView = (MaterialCalendarView) findViewById(R.id.calendarView);
        dates = new HashSet<>();
        calendar = Calendar.getInstance();
//        int maxday1 = Calendar.DAY_OF_MONTH;
//        Log.i("maxday1",String.valueOf(maxday1));
//        calendar = CalendarDay.from(2018,m,0);
        int currentmonth = calendar.get(Calendar.MONTH);
//        Toast.makeText(getApplicationContext(),lastday,Toast.LENGTH_LONG).show();
        Log.i("last day", String.valueOf(currentmonth));
        if(currentmonth<=m){

            setting_calandar();
        }else{

            setting_old_calandar();
        }

        Log.i("after calender months", String.valueOf(m));
        load_data_from_sever();
    }

    public  void setting_calandar(){
        materialCalendarView.state().edit()
                .setMinimumDate(CalendarDay.from(2018,m,1))
                .setMaximumDate(CalendarDay.from(2018,m,0))
                .commit();
        Log.i("setting calander",String.valueOf(m));
    }
    public  void setting_old_calandar(){
        materialCalendarView.state().edit()
                .setMinimumDate(CalendarDay.from(2018,m-1,1))
                .setMaximumDate(CalendarDay.from(2018,m,0))
                .commit();
        Log.i("setting old calander",String.valueOf(m-1));
    }
    public void load_data_from_sever(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/user/month";
        Log.i("url",url);
        jo = new JSONObject();
        int month = m+1;
        try {
            jo.put("username", getUsername);
            jo.put("mMonth",String.valueOf(month));
            Log.i("whichmonth",String.valueOf(month));
            Log.i("userid",getUsername);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                try {
                    // Create the root JSONObject from the JSON string.
                    dates.clear();
                    Log.i("RESpon",response.toString());
                    jsonArray  = response.optJSONArray("Data");
                    for (int i = 0; i < jsonArray.length() ; i++) {
                        JSONObject jsonobject = jsonArray.getJSONObject(i);
                        String day = jsonobject.getString("mDate");
                        int d = Integer.parseInt(day);
                        date = CalendarDay.from(2018,m,d);
                        dates.add(date);
                    }
                    eventDecorator = new EventDecorator(getResources().getColor(R.color.green),dates);
                    materialCalendarView.addDecorator(eventDecorator);

                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            Log.d("Verror", error.toString());
                            Toast.makeText(getApplicationContext(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();


                        }
                    }
                }
        );
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                10*1000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // add json request in volley request queue
        queue.add(jsonObjReq);

        //Toast.makeText(getApplicationContext(), "Thank You !!", Toast.LENGTH_LONG).show();

    }


}