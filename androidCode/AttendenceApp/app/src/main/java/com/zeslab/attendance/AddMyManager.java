package com.zeslab.attendance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class AddMyManager extends AppCompatActivity {

    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Userid = "userid";
    public static final String Username = "username";
    public static final String Groupname = "groupname";
    public static final String Managername = "managername";
    TextView title,manager_name;
    ImageView img;
    TextView manager;
    Button subuserButton;
    JSONObject jo,ji;
    String managerS,getUserid,getUsername,getGroupname,getManagername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_my_manager);
        img = (ImageView) findViewById(R.id.backmenu);
        title = (TextView) findViewById(R.id.title);
        manager_name = (TextView) findViewById(R.id.manager_name);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        getUsername = sharedpreferences.getString(Username,"");
        getGroupname = sharedpreferences.getString(Groupname,"");
        getManagername = sharedpreferences.getString(Managername,"");
        if(getManagername.equals("no")){
                manager_name.setText("Your manager is not set");
        }else {
            manager_name.setText("Your manager is "+getManagername);
        }
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AddMyManager.this,MySettings.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        title.setText("Add Your Manager");
        manager = (TextView) findViewById(R.id.managername);
        manager.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        subuserButton = (Button) findViewById(R.id.add_manager_button);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        subuserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                managerS=  manager.getText().toString();
                if(!managerS.equals(getUsername)) {
                    if (!managerS.equals(null)) {
                        subuserButton.setEnabled(false);
                        subuserButton.setText("working...");
                        set_My_Manager();
                    } else {
                        subuserButton.setEnabled(true);
                        subuserButton.setText("Add your Manager");
                        Toast.makeText(getApplicationContext(),
                                "please enter the Username", Toast.LENGTH_LONG).show();
                    }
                }else {
                    subuserButton.setEnabled(true);
                    subuserButton.setText("Add your Manager");
                    Toast.makeText(getApplicationContext(),
                            "It's you",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void set_My_Manager(){
        RequestQueue reqC = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/setMyManager";
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        getUserid = sharedpreferences.getString(Userid, "");

        try {
            if(jo == null){
                jo = new JSONObject();
            }
            jo.put("User_Id",getUserid);
            jo.put("username",getUsername);
            jo.put("managerId",managerS);
            jo.put("groupName",getGroupname);

            Log.i("JSONOBject","group"+getGroupname+" :username"+getUsername+":managerid"+managerS);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest str_req = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                /////////RESPONSE JSON STORE START
                String respn = response.toString();
                try {
                    boolean result = response.getBoolean("result");
                    Log.i("booleanres", String.valueOf(result));
                    if(result){
                        subuserButton.setEnabled(true);
                        subuserButton.setText("Add your Manager");
                        sharedpreferences.edit().putString(Managername,managerS).commit();
                        getManagername = sharedpreferences.getString(Managername,"");
                        manager_name.setText("Your manager is "+getManagername);
                        Toast.makeText(getApplicationContext(),
                                "Manager addition successful", Toast.LENGTH_LONG).show();
                    }else{
                        subuserButton.setEnabled(true);
                        subuserButton.setText("Add your Manager");
                        Toast.makeText(getApplicationContext(),
                                "Manager addition failed", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        subuserButton.setEnabled(true);
                        subuserButton.setText("Add your Manager");
                        Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_LONG).show();
                    }
                });
        reqC.add(str_req);
    }
}
