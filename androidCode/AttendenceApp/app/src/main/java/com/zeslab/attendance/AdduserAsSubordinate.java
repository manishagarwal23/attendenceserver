package com.zeslab.attendance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class AdduserAsSubordinate extends AppCompatActivity {
    TextView subuser;
    Button subuserButton;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Userid = "userid";
    public static final String Username = "username";
    public static final String Groupname = "groupname";
    String getUserId,subuserS,getUsername,getgroupname;
    JSONObject jo,ji;
    TextView title;
    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adduserassubordinate);
        img = (ImageView) findViewById(R.id.backmenu);
        title = (TextView) findViewById(R.id.title);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        getUsername = sharedpreferences.getString(Username,"");
        getUserId = sharedpreferences.getString(Userid,"");
        getgroupname = sharedpreferences.getString(Groupname,"");


        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent imanager = new Intent(AdduserAsSubordinate.this, MySettings.class );
                imanager.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(imanager);
            }
        });
        title.setText("Add Your Subordinate");
        subuser = (TextView) findViewById(R.id.subusername);
        subuser.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        subuserButton = (Button) findViewById(R.id.add_subuser_button);
        subuserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            subuserS=  subuser.getText().toString();
                if(!subuserS.equals(getUsername)){

                        if(!subuserS.equals(null)){
                            subuserButton.setEnabled(false);
                            subuserButton.setText("working...");
                            set_Your_subordinate();
                        }else {
                            subuserButton.setEnabled(true);
                            subuserButton.setText("Add your subordinate");
                            Toast.makeText(getApplicationContext(),
                                    "please enter the Username",Toast.LENGTH_LONG).show();
                        }
                }else if(subuserS.equals(getUsername)){

                    Toast.makeText(getApplicationContext(),
                            "It's you",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void set_Your_subordinate(){
        RequestQueue reqC = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/setMySubordinate";
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);


        try {
            if(jo == null){
                jo = new JSONObject();
            }
            jo.put("User_Id",getUserId);
            jo.put("subordinateId",subuserS);
            jo.put("username",getUsername);
            jo.put("groupName",getgroupname);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest str_req = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                /////////RESPONSE JSON STORE START
                String respn = response.toString();


                try {
                    boolean result = response.getBoolean("result");
                    Log.i("booleanres", String.valueOf(result));
                    if(result){
                        Toast.makeText(getApplicationContext(),
                                "subordinate addition sucessfull", Toast.LENGTH_LONG).show();
                        subuserButton.setEnabled(true);
                        subuserButton.setText("Add your subordinate");
                    }else{
                        Toast.makeText(getApplicationContext(),
                                "subordinate addition failed", Toast.LENGTH_LONG).show();
                        subuserButton.setEnabled(true);
                        subuserButton.setText("Add your subordinate");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        subuserButton.setEnabled(true);
                        subuserButton.setText("Add your subordinate");

                        Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_LONG).show();
                    }
                });
        reqC.add(str_req);
    }
}
