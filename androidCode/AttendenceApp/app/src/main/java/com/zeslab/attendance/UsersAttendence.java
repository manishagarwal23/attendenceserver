package com.zeslab.attendance;

import android.app.VoiceInteractor;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UsersAttendence extends AppCompatActivity {
    public UsersAttendence(){

    }

    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Userid = "userid";
    public static final String WhichMonth = "whichmonth";
    public static final String Useridofmanager = "useridofmanager";


    String getUserid,respn,getWhichMonth;
    JSONObject jo;
    JSONArray jsonArray;
    private List<AttendenceData> mDataset;
    private static RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private AttendenceRecyclerview adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_attendence);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        getUserid = sharedpreferences.getString(Useridofmanager,"");
        getWhichMonth = sharedpreferences.getString(WhichMonth,"");
        mDataset  = new ArrayList<>();
        recyclerView =findViewById(R.id.users_attendence);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);

        adapter = new AttendenceRecyclerview(this.getApplicationContext(), mDataset);
        recyclerView.setAdapter(adapter);
        insert_data_into_database();

    }

    public void insert_data_into_database(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/user/month";

        Log.i("url",url);
        jo = new JSONObject();
        try {
            jo.put("User_Id", getUserid);
            jo.put("mMonth",getWhichMonth);
            Log.i("whichmonth",getWhichMonth);
            Log.i("userid",getUserid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                try {
                        // Create the root JSONObject from the JSON string.
                    Log.i("RESpon",response.toString());
                    jsonArray = response.optJSONArray("Data");
                        mDataset.clear();
                        adapter.notifyDataSetChanged();
                        for (int i = 0; i < jsonArray.length() ; i++) {
                            JSONObject jsonobject = jsonArray.getJSONObject(i);
                            String in_Date = jsonobject.getString("in_Date");
                            String total_Time = jsonobject.getString("total_Time");
                            AttendenceData data = new AttendenceData(in_Date,total_Time);
                            mDataset.add(data);
                            adapter.notifyDataSetChanged();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            Log.d("Verror", error.toString());
                            Toast.makeText(getApplicationContext(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();


                        }
                    }
                }
        );
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                10*1000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // add json request in volley request queue
        queue.add(jsonObjReq);

        //Toast.makeText(getApplicationContext(), "Thank You !!", Toast.LENGTH_LONG).show();



    }

}
