package com.zeslab.attendance;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.speech.tts.UtteranceProgressListener;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.internal.Utility;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.util.Calendar;
import java.util.Map;

/**
 * Created by kumar on 25-04-2018.
 */

public class Myservices extends Service {
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Userid = "userid";
    public static final String IsLogin = "islogin";
    public static final String Officerange = "officerange";
    public static final String Officelocation = "Officelocation";
    public static final String Officelat = "Officelat";
    public static final String Officelong = "Officelong";
    public static final String Userlat = "userlat";
    public static final String Userlong = "userlong";
    public static final String Automatic = "auto";
    public static final String Automatictext = "autotext";
    public static final String Actiontoperform = "actiontoperform";


    LocationTracker locationTracker;
    double latitude = 0.0, longitude = 0.0;
    Location user_location,office_location;
    float distance = 0;
    int getOfficelat, getOfficelong;
    String getActiontoperform;
    @Override
    public void onCreate() {
        super.onCreate();
    }
    int count = 1;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        boolean getislogin = sharedpreferences.getBoolean(IsLogin,false);
        Log.i("islogin:::",String.valueOf(getislogin)+"is login");
        if (intent != null) {

            getActiontoperform = sharedpreferences.getString(Actiontoperform,"");
            boolean auto = sharedpreferences.getBoolean(Automatic,false);
            if(auto) {

                getOfficelat = sharedpreferences.getInt(Officelat, 0);
                getOfficelong = sharedpreferences.getInt(Officelong, 0);
                office_location = new Location("office_location");
                office_location.setLatitude(getOfficelat / 1e6);
                office_location.setLongitude(getOfficelong / 1e6);

                locationTracker = new LocationTracker(this);
                //////////////////////////////  ******** one try
                if (locationTracker.canGetLocation()) {
                    latitude = locationTracker.getLatitude();
                    longitude = locationTracker.getLongitude();
                    user_location = new Location("user_location");
                    user_location.setLatitude(latitude);
                    user_location.setLongitude(longitude);
                    distance =user_location.distanceTo(office_location);
                    String dis = String.valueOf(distance);
                    utlitis.getInstance().distance.setText(dis);
//                    Toast.makeText(getApplicationContext(), "distance"+distance, Toast.LENGTH_LONG).show();
                    if(distance<=50){
//                        Toast.makeText(this,"you are in the office: "+distance,Toast.LENGTH_LONG).show();
                       if(getActiontoperform.equals("IN")) {
//                           Toast.makeText(getApplicationContext(), "auto"+count, Toast.LENGTH_LONG).show();
                           utlitis.getInstance().making_JSONObject();

                       }

                    }else {
                        if(getActiontoperform.equals("OUT")) {
                            utlitis.getInstance().making_JSONObject();
                        }
//                        Toast.makeText(this,"you are out from the office: "+distance,Toast.LENGTH_LONG).show();
                    }
                    Log.i("LocationTracker ser", "LatLong: " + latitude + ", " + longitude);
                    Log.i("location Tracker 1 ser", "user" + user_location.toString());
                } else {
                    Log.i("LocationTracker ser", "Unable to get location");
                }

            }

            utlitis u = utlitis.getInstance();
            MyMaps m =  u.maps;
            GoogleMap gm = u.googleMap;
            if(gm != null){
                m.myMapReady(gm);
            }
              // error in this line 59
//            utlitis.getInstance().maps.myMapReady(utlitis.getInstance().googleMap);
        Toast.makeText(getApplicationContext(), "Alarm..."+count, Toast.LENGTH_LONG).show();
//        Log.i("Alarm1","Alarm"+ Calendar.getInstance().getTime());
//        Toast.makeText(this,"services started",Toast.LENGTH_LONG).show();
//            Log.i("Alarm1", "Myservices" + Calendar.getInstance().getTime());
//            count++;
        }
            return Service.START_STICKY;

    }

    @Override
    public void onDestroy() {

        Toast.makeText(this,"services stoped",
                Toast.LENGTH_LONG).show();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
//        Toast.makeText(getApplicationContext(), "Alarm...", Toast.LENGTH_LONG).show();
//        Log.i("Alarm1","Alarm"+ Calendar.getInstance().getTime());
        return null;
    }

    void stopMyService() {
        stopForeground(true);
        stopSelf();

    }
}