package com.zeslab.attendance;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;


public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Userid = "userid";
    public static final String Username = "username";
    public static final String IsLogin = "islogin";
    public static final String Signuporloginpage = "signuporloginpage";
    public static final String Groupname = "groupname";
    public static final String groupcheck = "groupcheck";
    public ProgressBar progressBar;
    String Id;
    String respn;
    JSONObject ji;
    EditText  username, email, password,confirmPassword,groupname;
    String  usernameS, emailS, passwordS,confirmPasswordS,groupnameS;
    String fb_token,fb_ID;
    Button signup,showUsers;
    LoginButton loginbutton;
    TextView login_text;
    CheckBox aggrement;
    private static final String EMAIL = "email";
    String ip_port;
    String url;
    CallbackManager callbackManager;
    String at;
    View view;
    RelativeLayout relative_Signup;
    String ua;


//    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_main);
        progressBar = findViewById(R.id.progressBar0);
        aggrement =(CheckBox) findViewById(R.id.aggrement);
//        relative_Signup = (RelativeLayout) findViewById(R.id.relative_Signup);
        ip_port = getString(R.string.url);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        boolean loginCheck = sharedpreferences.getBoolean(IsLogin,false);
        if(loginCheck== true){
            mtdisLogin();
        }else{
            String getSignuporLoginPage = sharedpreferences.getString(Signuporloginpage,"");
            if(getSignuporLoginPage.equals("Login")){
                Intent i = new Intent(MainActivity.this,SignIn.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        }

        ua =new WebView(this).getSettings().getUserAgentString();
        login_text = (TextView) findViewById(R.id.login_text);
        login_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(Signuporloginpage, "Login"); // Storing string
                editor.commit();
                Intent i = new Intent(MainActivity.this,SignIn.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });
        username = findViewById(R.id.username);
        username.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        email = findViewById(R.id.email_ID);

        password = findViewById(R.id.password);
        confirmPassword = findViewById(R.id.confirmPassword);
        callbackManager = CallbackManager.Factory.create();
        loginbutton = findViewById(R.id.login_button);
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.zeslab.attendance",
                    PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loginbutton.setReadPermissions(Arrays.asList(EMAIL));
                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(LoginResult loginResult) {
//                               String token   =loginResult.getAccessToken().toString();
//                                at =  loginResult.getAccessToken();
                                at = AccessToken.getCurrentAccessToken().getToken();
//                                fb_token = at.getToken();
//                                fb_ID = at.getUserId();
                                Log.i("Mytoken" ,at);
//                                Log.i("userid",at.getUserId());
                                GraphRequest request = GraphRequest.newMeRequest(
                                        loginResult.getAccessToken(),
                                        new GraphRequest.GraphJSONObjectCallback() {

                                            @Override
                                            public void onCompleted(JSONObject object, GraphResponse response) {
                                                Log.v("Main", response.toString());
                                                setProfileToView(object);
                                                Log.i("objectfb",object.toString());
                                            }
                                        });
                                Bundle parameters = new Bundle();
                                parameters.putString("fields", "id,name,email,gender, birthday");
                                request.setParameters(parameters);
                                request.executeAsync();

//                                mtdisLogin();
                            }

                            @Override
                            public void onCancel() {
                                // App code
                                Log.i("onCancel", "called");
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                // App code
                                Log.i("onError", "called" + exception.toString());
                            }
                        });
                }
        });

        signup  = findViewById(R.id.signupbutton);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                url = ip_port+"/signupIfNew";
                checkUsername_and_Password(view);
            }
        });

        }catch (Exception e){
            sharedpreferences.edit().clear().apply();
        }
    }



    public void insert_data_into_database(){
        RequestQueue reqC = Volley.newRequestQueue(this);

        progressBar.setVisibility(View.VISIBLE);
        usernameS = username.getText().toString();
        emailS = email.getText().toString();
        passwordS = password.getText().toString();
        confirmPasswordS = confirmPassword.getText().toString();
//        groupnameS = groupname.getText().toString();
        JSONObject jo = new JSONObject();
        try {
            jo.put("username", usernameS);
            jo.put("email", emailS);
            jo.put("password", passwordS);
            jo.put("confirmPassword",confirmPasswordS);
            jo.put("fb_ID", fb_ID);
            jo.put("fb_token", fb_token);
            jo.put("groupName","TEST");
            jo.put("userAgent","1");
            Log.i("sendingOnsignup",jo.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest str_req = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                /////////RESPONSE JSON STORE START////////
                respn = response.toString();
                Log.i("res1", String.valueOf(respn));
                try {
                    boolean result = response.getBoolean("result");
                    Log.i("booleanres", String.valueOf(result));
                    if(result){
                        savingId();
                    }else{

                        Toast.makeText(getApplicationContext(),
                                "signup failed", Toast.LENGTH_LONG).show();
//                        relative_Signup.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
//                    relative_Signup.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        relative_Signup.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(getApplicationContext(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
                    }


                });

        // add json request in volley request queue
        reqC.add(str_req);

        //Toast.makeText(getApplicationContext(), "Thank You !!", Toast.LENGTH_LONG).show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
    /////////////////   methods for getting data from fb button
    private void setProfileToView(JSONObject jsonObject) {

            String getemail = jsonObject.optString("email");
            if(getemail == null || getemail.length()<1){
                String getId= jsonObject.optString("id");
                String setemail = getId+"@fb.com";
                email.setText(setemail);
                String emailToUsername = email.getText().toString();
                String usernamecreated = emailToUsername.split("@")[0];
                username.setText(usernamecreated);
            }else {
                email.setText(getemail);
                String emailToUsername = email.getText().toString();
                String usernamecreated = emailToUsername.split("@")[0];
                username.setText(usernamecreated);
            }

            password.setText("1234");

            url = ip_port+"/login/facebook";
            insert_data_into_database();


    }


    public void savingId(){
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);

        try {
            ji = new JSONObject(respn);
           JSONObject ji1 =  ji.getJSONObject("Data");
            Id = ji1.getString("_id");
            String username = ji1.getString("username");
            String groupname =  ji1.getString("groupName");

                sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(Userid, Id); // Storing string
                editor.putString(Username, username);
                editor.putString(Groupname,groupname);
                editor.putBoolean(IsLogin,true);
                editor.commit();

//                   //////   have to make true so that user intent to mymaps activity
                String show1 = sharedpreferences.getString(Userid,"");
                Log.i("show",show1);

        } catch (JSONException e) {
            e.printStackTrace();
            progressBar.setVisibility(View.INVISIBLE);
            Log.i("show",e.toString());
            Toast.makeText(getApplicationContext(),"error while saving id",
                    Toast.LENGTH_LONG).show();
        }

        intentto_groupname();

    }

    public void intentto_groupname(){

        Intent i1 = new Intent(MainActivity.this, Groupname.class);
        i1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i1);
        finish();
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void mtdisLogin(){

        Intent i1 = new Intent(MainActivity.this, MyMaps.class);
        i1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i1);
        finish();
        progressBar.setVisibility(View.INVISIBLE);
    }
//    public void startAlert() {
////        EditText text = (EditText) findViewById(R.id.time);
////        long i = Integer.parseInt(text.getText().toString());
//        Intent intent = new Intent(this, Myservices.class);
////        PendingIntent pendingIntent = PendingIntent.getBroadcast(
////                this.getApplicationContext(), 234324243, intent, 0);
//        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 0, intent, 0);
//        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
//        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
//                + (2 * 1000),15*1000, pendingIntent);
//        Toast.makeText(this, "Alarm set in " + 2 + " seconds", Toast.LENGTH_LONG).show();
//
//    }
private static boolean isValidEmail(String email) {
    return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
}
    public void checkUsername_and_Password(View view){
        usernameS = username.getText().toString();
        emailS = email.getText().toString();
        passwordS = password.getText().toString();
        confirmPasswordS = confirmPassword.getText().toString();
        if(usernameS.length()>2){         // email part have to modify
            if(emailS.length()>2 && isValidEmail(emailS)) {
                if (passwordS.length() > 3) {
                    if (passwordS.equals(confirmPasswordS)) {
                        if(aggrement.isChecked()){
                            progressBar.setVisibility(View.VISIBLE);
                            insert_data_into_database();
                        }else {
                            Toast.makeText(getApplicationContext(),
                                    "Please check the agreement", Toast.LENGTH_LONG).show();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(),
                                "password doesn't match", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(),
                            "weak password", Toast.LENGTH_LONG).show();
                }
            }else {
                Toast.makeText(getApplicationContext(),
                        "Please enter valid email",Toast.LENGTH_LONG).show();
            }
        }else{
                Toast.makeText(getApplicationContext(),
                        "Please enter different username",Toast.LENGTH_LONG).show();
            }
    }
}


