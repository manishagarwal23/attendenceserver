package com.zeslab.attendance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by kumar on 16-10-2017.
 *
 */

public class ManagerRecyclerviewAdapter extends
        RecyclerView.Adapter<ManagerRecyclerviewAdapter.ViewHolder> {
    SharedPreferences sharedPreferences;
    public static final String mypreference = "mypref";
    public static final String Useridofsubordinate = "useridofsubordinate";
    public static final String Usernameofsubordinate = "usernameofsubordinate";

    String getuserid_of_subordinate,getusername_of_subordinate;

    private Context context;
    private List<UserData> users;



    public ManagerRecyclerviewAdapter(Context context, List<UserData> users)  {
        this.context = context;
        this.users = users;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.managercardview,
                parent,false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.username_of_subordinate.setText(users.get(position).getUser());


    }

    @Override
    public int getItemCount() {
        if(users == null)
            return 0;
        return users.size();
    }

    public  class ViewHolder extends  RecyclerView.ViewHolder {

        TextView username_of_subordinate;


        public ViewHolder(View itemView) {
            super(itemView);
            username_of_subordinate = itemView.findViewById(R.id.username_of_subordinate);

            username_of_subordinate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    getusername_of_subordinate = (String) username_of_subordinate.getText().toString();

                    Log.i("getusername of sub",getusername_of_subordinate);
                    sharedPreferences =  v.getContext().getSharedPreferences(mypreference, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(Usernameofsubordinate, getusername_of_subordinate);
                    editor.commit();
                    Log.i("getusername of sub",getusername_of_subordinate);

//                    String userid = sharedPreferences.getString(getuserid_of_subordinate,"");
                    String username = sharedPreferences.getString(getusername_of_subordinate,"");
//                    Log.i("userid",userid);
//                    Log.i("useername",username);
//                    Toast.makeText(v.getContext(),"userid"+userid,Toast.LENGTH_SHORT).show();
                    Toast.makeText(v.getContext(),"username"+username,Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(v.getContext(),SuordinateDetail.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    v.getContext().startActivity(i);
                }
            });
        }


    }
}