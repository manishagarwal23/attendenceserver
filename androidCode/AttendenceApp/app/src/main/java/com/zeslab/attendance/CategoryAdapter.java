package com.zeslab.attendance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by kumar on 16-10-2017.
 *
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private Context context;
    private List<CatoData> cities;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Category = "category";


    public CategoryAdapter(Context context, List<CatoData> cities)  {
        this.context = context;
        this.cities = cities;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview,parent,false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.text.setText(cities.get(position).getText());
        holder.mobile.setText(cities.get(position).getMobile());
        holder.email.setText(cities.get(position).getEmail());
        holder.password.setText(cities.get(position).getPassword());

    }

    @Override
    public int getItemCount() {
        if(cities == null)
            return 0;
        return cities.size();
    }

    public  class ViewHolder extends  RecyclerView.ViewHolder {

        TextView text, mobile, email, password;


        public ViewHolder(View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.text);
            mobile= itemView.findViewById(R.id.mobile);
            email= itemView.findViewById(R.id.email);
            password= itemView.findViewById(R.id.password);
//            categorytext.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {

//                    String cid = cid_textview.getText().toString();
//                    sharedpreferences = context.getSharedPreferences(mypreference,
//                            Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editor = sharedpreferences.edit();
//                    editor.putString(Category,cid);
//                    editor.commit();
//                    Intent intent = new Intent(view.getContext(),MainActivity.class );
//
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    view.getContext().startActivity(intent);
//                    if(getActivity() != null) {
//                        viewPager.setCurrentItem(0); // FIRST fragment index is 0
////                                            Toast.makeText(getContext(), cato_id, Toast.LENGTH_SHORT).show();
////                                            Log.i("clicked ", " clicked on "+ categoryname);
//                    }

                }
//            });
//
//        }


    }
}