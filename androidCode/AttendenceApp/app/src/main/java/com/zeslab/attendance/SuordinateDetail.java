package com.zeslab.attendance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SuordinateDetail extends AppCompatActivity {

    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Userid = "userid";
    public static final String Username = "username";
    public static final String WhichMonth = "whichmonth";
    public static final String WhichYear = "whichyear";
    public static final String Useridofsubordinate = "useridofsubordinate";
    public static final String Usernameofsubordinate = "usernameofsubordinate";
    public static final String Jan = "jan";
    public static final String Feb = "feb";
    public static final String Mar = "mar";
    public static final String Apr = "apr";
    public static final String May = "may";
    public static final String Jun = "jun";
    public static final String Jul = "jul";
    public static final String Aug = "aug";
    public static final String Sep = "sep";
    public static final String Oct = "oct";
    public static final String Nov = "nov";
    public static final String Dec = "Dec";
    public static final String Workdays = "workdays";
    public static final String Leavedays = "leavedays";
    JSONObject jo;
    JSONArray jsonArray;
    public String month,getUserid,work_day;
    ImageView img;
    boolean popup;
    String totalworkdays,getUseridofmanager;
    String getIsmanagerwatching;
    TextView janT,febT,marT,aprT,mayT,junT,julT,augT,sepT,octT,novT,decT;
    int days = 0,i;
    TextView userNameTitle;
    String getWhichMonth;
    UsersAttendence usersAttendence = new UsersAttendence();
    TextView total_days_work;
    TextView total_leave;



    ////methods for months popup
    public void months_ID(View view){
        if(view.getId() == R.id.jan){
            month =  "0";
            getMonth(view);
//            Toast.makeText(getApplicationContext(),"jan",Toast.LENGTH_LONG).show();

        }if(view.getId() == R.id.feb){
            month =  "1";
            getMonth(view);
//            Toast.makeText(getApplicationContext(),"feb",Toast.LENGTH_LONG).show();

        }if(view.getId() == R.id.mar){
            month =  "2";
            getMonth(view);
//            Toast.makeText(getApplicationContext(),"mar",Toast.LENGTH_LONG).show();

        }if(view.getId() == R.id.apr){
            month =  "3";
            getMonth(view);
//            Toast.makeText(getApplicationContext(),"apr",Toast.LENGTH_LONG).show();

        }if(view.getId() == R.id.may){
            month =  "4";
            getMonth(view);
//            Toast.makeText(getApplicationContext(),"may",Toast.LENGTH_LONG).show();

        }if(view.getId() == R.id.jun){
            month =  "5";
            getMonth(view);
//            Toast.makeText(getApplicationContext(),"jun",Toast.LENGTH_LONG).show();

        }if(view.getId() == R.id.jul){
            month =  "6";
            getMonth(view);
//            Toast.makeText(getApplicationContext(),"jul",Toast.LENGTH_LONG).show();

        }if(view.getId() == R.id.aug){
            month =  "7";
            getMonth(view);
//            Toast.makeText(getApplicationContext(),"aug",Toast.LENGTH_LONG).show();

        }if(view.getId() == R.id.sep){
            month =  "8";
            getMonth(view);
//            Toast.makeText(getApplicationContext(),"sep",Toast.LENGTH_LONG).show();

        }if(view.getId() == R.id.oct){
            month =  "9";
            getMonth(view);
//            Toast.makeText(getApplicationContext(),"oct",Toast.LENGTH_LONG).show();

        }if(view.getId() == R.id.nov){
            month =  "10";
            getMonth(view);
//            Toast.makeText(getApplicationContext(),"nov",Toast.LENGTH_LONG).show();

        }if(view.getId() == R.id.dec){
            month =  "11";
            getMonth(view);
//            Toast.makeText(getApplicationContext(),"dec",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subordinate_details);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
//        rightMenu = findViewById(R.id.rightmenu);
//        rightMenu.setVisibility(View.GONE);
        img = (ImageView) findViewById(R.id.backmenu);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent imanager = new Intent(SuordinateDetail.this, ManagerActivity.class );
                imanager.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(imanager);
            }
        });
        String title = sharedpreferences.getString(Usernameofsubordinate,"");
        userNameTitle = (TextView) findViewById(R.id.title) ;
        userNameTitle.setText(title);

//        total_leave = (TextView) findViewById(R.id.total_leave);
        total_days_work= (TextView) findViewById(R.id.total_days_work);

        janT = (TextView) findViewById(R.id.janT);
        febT = (TextView) findViewById(R.id.febT);
        marT = (TextView) findViewById(R.id.marT);
        aprT = (TextView) findViewById(R.id.aprT);
        mayT = (TextView) findViewById(R.id.mayT);
        junT = (TextView) findViewById(R.id.junT);
        julT = (TextView) findViewById(R.id.julT);
        augT = (TextView) findViewById(R.id.augT);
        sepT = (TextView) findViewById(R.id.sepT);
        octT = (TextView) findViewById(R.id.octT);
        novT = (TextView) findViewById(R.id.novT);
        decT = (TextView) findViewById(R.id.decT);

        janT.setText(sharedpreferences.getString(Jan,""));
        febT.setText(sharedpreferences.getString(Feb,""));
        marT.setText(sharedpreferences.getString(Mar,""));
        aprT.setText(sharedpreferences.getString(Apr,""));
        mayT.setText(sharedpreferences.getString(May,""));
        junT.setText(sharedpreferences.getString(Jun,""));
        julT.setText(sharedpreferences.getString(Jul,""));
        augT.setText(sharedpreferences.getString(Aug,""));
        sepT.setText(sharedpreferences.getString(Sep,""));
        octT.setText(sharedpreferences.getString(Oct,""));
        novT.setText(sharedpreferences.getString(Nov,""));
        decT.setText(sharedpreferences.getString(Dec,""));

        call_1_api_for_attendence();


    load_data_for_work_days();

    }

    public void getMonth(View view){
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(WhichMonth, month); // Storing string
        editor.commit();
        Intent i = new Intent(this, Subordinate_calendar.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    private void load_data_for_work_days() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/year/worked";
//        simpleProgressBar.setVisibility(View.VISIBLE);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        String getUserid1 = sharedpreferences.getString(Usernameofsubordinate, "");
        jo = new JSONObject();
        try {
            jo.put("username", getUserid1);
            jo.put("mYear", "2018");

            Toast.makeText(getApplicationContext(),"your userid" + getUserid1 ,Toast.LENGTH_LONG).show();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.i("getting data of year",response.toString());
                    boolean result = response.getBoolean("result");
                    if(result){
                        String yearWork = response.getString("Data");
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Workdays, yearWork); // Storing string
                        editor.commit();
                        String work_day = sharedpreferences.getString(Workdays,"");
                        total_days_work.setText(work_day+" Days");
//                        int total_leaves = Integer.parseInt(work_day);
//
//                        int value = days - total_leaves;
//                        total_leave.setText(String.valueOf(value)+" Days");

                    }else{
                        Toast.makeText(getApplicationContext(),"data not " +
                                "found",Toast.LENGTH_LONG).show();
                    }
                        } catch (JSONException e) {
                            e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"catch block of year",Toast.LENGTH_LONG).show();
//                            simpleProgressBar.setVisibility(View.INVISIBLE);

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
//                            Log.d(TAG, error.toString());
                            Toast.makeText(getApplicationContext(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
//
                        }
                    }
                }
        );
        requestQueue.add(jsonObjReq);
    }

    ///////// function for saving date and setting dates
    public void setting_months_values_and_saving_value(int m, String daycount){
        Log.i("month value ",String.valueOf(m));
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if(m==1){
            editor.putString(Jan, daycount); // Storing string
            editor.commit();
            janT.setText(sharedpreferences.getString(Jan, ""));

        }else if(m==2){
            editor.putString(Feb, daycount); // Storing string
            editor.commit();
            febT.setText(sharedpreferences.getString(Feb, ""));

        }else if(m==3){
            editor.putString(Mar, daycount); // Storing string
            editor.commit();
            marT.setText(sharedpreferences.getString(Mar, ""));

        }else if(m==4){
            editor.putString(Apr, daycount); // Storing string
            editor.commit();
            aprT.setText(sharedpreferences.getString(Apr, ""));

        }else if(m==5){
            editor.putString(May, daycount); // Storing string
            editor.commit();
            mayT.setText(sharedpreferences.getString(May, ""));

        }else if(m==6){
            editor.putString(Jun, daycount); // Storing string
            editor.commit();
            junT.setText(sharedpreferences.getString(Jun, ""));

        }else if(m==7){
            editor.putString(Jul, daycount); // Storing string
            editor.commit();
            julT.setText(sharedpreferences.getString(Jul, ""));

        }else if(m==8){
            editor.putString(Aug, daycount); // Storing string
            editor.commit();
            augT.setText(sharedpreferences.getString(Aug, ""));

        }else if(m==9){
            editor.putString(Sep, daycount); // Storing string
            editor.commit();
            sepT.setText(sharedpreferences.getString(Sep, ""));

        }else if(m==10){
            editor.putString(Oct, daycount); // Storing string
            editor.commit();
            octT.setText(sharedpreferences.getString(Oct, ""));

        }else if(m==11){
            editor.putString(Nov, daycount); // Storing string
            editor.commit();
            novT.setText(sharedpreferences.getString(Nov, ""));

        }else if(m==12){
            editor.putString(Dec, daycount); // Storing string
            editor.commit();
            decT.setText(sharedpreferences.getString(Dec, ""));

        }
    }



    //Jan
    private void call_1_api_for_attendence() {
        for(i = 1; i<=12;i++) {
            final RequestQueue requestQueue = Volley.newRequestQueue(this);
            String ip_port = getString(R.string.url);
            String url = ip_port+"/month/worked";
            //        simpleProgressBar.setVisibility(View.VISIBLE);
            sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                    Context.MODE_PRIVATE);
            String getUserid1 = sharedpreferences.getString(Usernameofsubordinate, "");

            jo = new JSONObject();
            try {
                jo.put("username", getUserid1);
                jo.put("mMonth", i);
                jo.put("mYear", "2018");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jo, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {

                        Log.i("getting data of month", response.toString());
                        boolean result = response.getBoolean("result");
                        if (result) {
                            HashMap<Integer, Integer> map1 = new HashMap<>();
                            JSONArray j1 = response.getJSONArray("Days");
                            String month = response.getString("Month");
                            for(int i=0;i<j1.length();i++){
                                JSONObject j2 = j1.getJSONObject(i);
                                String day = j2.getString("mDate");
                                int day1 =  Integer.parseInt(day);
                                map1.put(day1,1);
                            }


//                            JSONObject jsonObject =  jsonArray.getJSONObject(0);
//                            String month = jsonObject.getString("mMonth");

//                            String month = response.getString("Month");
//                               Log.i("day value",daycount);
                            Log.i("mMonth value",month);
//                            Log.i("getting months:",String.valueOf(i) +" "+ daycount);
                            int d = map1.size();
                            days = days + d;
                            total_days_work.setText(days +" Days");
                            String daycount = String.valueOf(d);
                            int mon = Integer.parseInt(month);
                            setting_months_values_and_saving_value(mon,daycount);/// string factory function


                        } else {
                            Toast.makeText(getApplicationContext(), "data not " +
                                    "found", Toast.LENGTH_LONG).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "catch block of month", Toast.LENGTH_LONG).show();
                        //                            simpleProgressBar.setVisibility(View.INVISIBLE);

                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error != null) {
                                //                            Log.d(TAG, error.toString());
                                //Toast.makeText(getActivity(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
                                //

                            }
                        }
                    }
            );
            requestQueue.add(jsonObjReq);
        }
    }
    //feb
    private void call_2_api_for_attendence() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/month/worked";
        //        simpleProgressBar.setVisibility(View.VISIBLE);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        String getUserid1 = sharedpreferences.getString(Usernameofsubordinate, "");
        jo = new JSONObject();
        try {
            jo.put("username", getUserid1);
            jo.put("mMonth","February");
            jo.put("mYear","2018");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                try {

                    Log.i("getting data of year",response.toString());
                    boolean result = response.getBoolean("result");
                    if(result){
                        String daycount = response.getString("Data");

                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Feb, daycount); // Storing string
                        editor.commit();
                        febT.setText(sharedpreferences.getString(Feb,""));
                    }else{
                        Toast.makeText(getApplicationContext(),"data not " +
                                "found",Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"catch block of month",Toast.LENGTH_LONG).show();
                    //                            simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            //                            Log.d(TAG, error.toString());
                            //Toast.makeText(getActivity(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
                            //
                        }
                    }
                }
        );
        requestQueue.add(jsonObjReq);
    }
    //march
    private void call_3_api_for_attendence() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/month/worked";
        //        simpleProgressBar.setVisibility(View.VISIBLE);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        String getUserid1 = sharedpreferences.getString(Usernameofsubordinate, "");
        jo = new JSONObject();
        try {
            jo.put("username", getUserid1);
            jo.put("mMonth","March");
            jo.put("mYear","2018");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                try {

                    Log.i("getting data of year",response.toString());
                    boolean result = response.getBoolean("result");
                    if(result){
                        String daycount = response.getString("Data");
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Mar, daycount); // Storing string
                        editor.commit();
                        marT.setText(sharedpreferences.getString(Mar,""));
                    }else{
                        Toast.makeText(getApplicationContext(),"data not " +
                                "found",Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"catch block of month",Toast.LENGTH_LONG).show();
                    //                            simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            //                            Log.d(TAG, error.toString());
                            //Toast.makeText(getActivity(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
                            //
                        }
                    }
                }
        );
        requestQueue.add(jsonObjReq);
    }
    //april
    private void call_4_api_for_attendence() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/month/worked";
        //        simpleProgressBar.setVisibility(View.VISIBLE);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        String getUserid1 = sharedpreferences.getString(Usernameofsubordinate, "");
        jo = new JSONObject();
        try {
            jo.put("username", getUserid1);
            jo.put("mMonth","April");
            jo.put("mYear","2018");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                try {

                    Log.i("getting data of year",response.toString());
                    boolean result = response.getBoolean("result");
                    if(result){
                        String daycount = response.getString("Data");
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Apr, daycount); // Storing string
                        editor.commit();
                        aprT.setText(sharedpreferences.getString(Apr,""));
                    }else{
                        Toast.makeText(getApplicationContext(),"data not " +
                                "found",Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"catch block of month",Toast.LENGTH_LONG).show();
                    //                            simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            //                            Log.d(TAG, error.toString());
                            //Toast.makeText(getActivity(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
                            //
                        }
                    }
                }
        );
        requestQueue.add(jsonObjReq);
    }
    //may
    private void call_5_api_for_attendence() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        String ip_port = getString(R.string.url);
        String url = ip_port+"/month/worked";
        //        simpleProgressBar.setVisibility(View.VISIBLE);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        String getUserid1 = sharedpreferences.getString(Usernameofsubordinate, "");
        jo = new JSONObject();
        try {
            jo.put("username", getUserid1);
            Log.i("User_Id_in_may",getUserid1);
            jo.put("mMonth","May");
            jo.put("mYear","2018");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.i("getting data of year",response.toString());
                    boolean result = response.getBoolean("result");
                    if(result){
                        String daycount = response.getString("Data");
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(May, daycount); // Storing string
                        editor.commit();
                        mayT.setText(sharedpreferences.getString(May,""));
                    }else{
                        Toast.makeText(getApplicationContext(),"data not " +
                                "found",Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"catch block of month",Toast.LENGTH_LONG).show();
                    //                            simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            //                            Log.d(TAG, error.toString());
                            //Toast.makeText(getActivity(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
                            //
                        }
                    }
                }
        );
        requestQueue.add(jsonObjReq);
    }
    //june
    private void call_6_api_for_attendence() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/month/worked";
        //        simpleProgressBar.setVisibility(View.VISIBLE);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        String getUserid1 = sharedpreferences.getString(Usernameofsubordinate, "");
        jo = new JSONObject();
        try {
            jo.put("username", getUserid1);
            jo.put("mMonth","June");
            jo.put("mYear","2018");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                try {

                    Log.i("getting data of year",response.toString());
                    boolean result = response.getBoolean("result");
                    if(result){
                        String daycount = response.getString("Data");
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Jun, daycount); // Storing string
                        editor.commit();
                        junT.setText(sharedpreferences.getString(Jun,""));
                    }else{
                        Toast.makeText(getApplicationContext(),"data not " +
                                "found",Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"catch block of month",Toast.LENGTH_LONG).show();
                    //                            simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            //                            Log.d(TAG, error.toString());
                            //Toast.makeText(getActivity(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
                            //
                        }
                    }
                }
        );
        requestQueue.add(jsonObjReq);
    }
    //jul
    private void call_7_api_for_attendence() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/month/worked";
        //        simpleProgressBar.setVisibility(View.VISIBLE);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        String getUserid1 = sharedpreferences.getString(Usernameofsubordinate, "");
        jo = new JSONObject();
        try {
            jo.put("username", getUserid1);
            jo.put("mMonth","July");
            jo.put("mYear","2018");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                try {

                    Log.i("getting data of year",response.toString());
                    boolean result = response.getBoolean("result");
                    if(result){
                        String daycount = response.getString("Data");
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Jul, daycount); // Storing string
                        editor.commit();
                        julT.setText(sharedpreferences.getString(Jul,""));
                    }else{
                        Toast.makeText(getApplicationContext(),"data not " +
                                "found",Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"catch block of month",Toast.LENGTH_LONG).show();
                    //                            simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            //                            Log.d(TAG, error.toString());
                            //Toast.makeText(getActivity(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
                            //
                        }
                    }
                }
        );
        requestQueue.add(jsonObjReq);
    }
    //aug
    private void call_8_api_for_attendence() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/month/worked";
        //        simpleProgressBar.setVisibility(View.VISIBLE);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        String getUserid1 = sharedpreferences.getString(Usernameofsubordinate, "");
        jo = new JSONObject();
        try {
            jo.put("username", getUserid1);
            jo.put("mMonth","August");
            jo.put("mYear","2018");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                try {

                    Log.i("getting data of year",response.toString());
                    boolean result = response.getBoolean("result");
                    if(result){
                        String daycount = response.getString("Data");
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Aug, daycount); // Storing string
                        editor.commit();
                        augT.setText(sharedpreferences.getString(Aug,""));
                    }else{
                        Toast.makeText(getApplicationContext(),"data not " +
                                "found",Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"catch block of month",Toast.LENGTH_LONG).show();
                    //                            simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            //                            Log.d(TAG, error.toString());
                            //Toast.makeText(getActivity(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
                            //
                        }
                    }
                }
        );
        requestQueue.add(jsonObjReq);
    }
    //sep
    private void call_9_api_for_attendence() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/month/worked";
        //        simpleProgressBar.setVisibility(View.VISIBLE);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        String getUserid1 = sharedpreferences.getString(Usernameofsubordinate, "");
        jo = new JSONObject();
        try {
            jo.put("username", getUserid1);
            jo.put("mMonth","September");
            jo.put("mYear","2018");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                try {

                    Log.i("getting data of year",response.toString());
                    boolean result = response.getBoolean("result");
                    if(result){
                        String daycount = response.getString("Data");
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Sep, daycount); // Storing string
                        editor.commit();
                        sepT.setText(sharedpreferences.getString(Sep,""));
                    }else{
                        Toast.makeText(getApplicationContext(),"data not " +
                                "found",Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"catch block of month",Toast.LENGTH_LONG).show();
                    //                            simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            //                            Log.d(TAG, error.toString());
                            //Toast.makeText(getActivity(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
                            //
                        }
                    }
                }
        );
        requestQueue.add(jsonObjReq);
    }
    //oct
    private void call_10_api_for_attendence() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/month/worked";
        //        simpleProgressBar.setVisibility(View.VISIBLE);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        String getUserid1 = sharedpreferences.getString(Usernameofsubordinate, "");
        jo = new JSONObject();
        try {
            jo.put("username", getUserid1);
            jo.put("mMonth","October");
            jo.put("mYear","2018");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                try {

                    Log.i("getting data of year",response.toString());
                    boolean result = response.getBoolean("result");
                    if(result){
                        String daycount = response.getString("Data");
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Oct, daycount); // Storing string
                        editor.commit();
                        octT.setText(sharedpreferences.getString(Oct,""));
                    }else{
                        Toast.makeText(getApplicationContext(),"data not " +
                                "found",Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"catch block of month",Toast.LENGTH_LONG).show();
                    //                            simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            //                            Log.d(TAG, error.toString());
                            //Toast.makeText(getActivity(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
                            //
                        }
                    }
                }
        );
        requestQueue.add(jsonObjReq);
    }
    //nov
    private void call_11_api_for_attendence() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/month/worked";
        //        simpleProgressBar.setVisibility(View.VISIBLE);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        String getUserid1 = sharedpreferences.getString(Usernameofsubordinate, "");
        jo = new JSONObject();
        try {
            jo.put("username", getUserid1);
            jo.put("mMonth","November");
            jo.put("mYear","2018");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                try {

                    Log.i("getting data of year",response.toString());
                    boolean result = response.getBoolean("result");
                    if(result){
                        String daycount = response.getString("Data");
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Nov, daycount); // Storing string
                        editor.commit();
                        novT.setText(sharedpreferences.getString(Nov,""));
                    }else{
                        Toast.makeText(getApplicationContext(),"data not " +
                                "found",Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"catch block of month",Toast.LENGTH_LONG).show();
                    //                            simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            //                            Log.d(TAG, error.toString());
                            //Toast.makeText(getActivity(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
                            //
                        }
                    }
                }
        );
        requestQueue.add(jsonObjReq);
    }
    //dec
    private void call_12_api_for_attendence() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/month/worked";
        //        simpleProgressBar.setVisibility(View.VISIBLE);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        String getUserid1 = sharedpreferences.getString(Usernameofsubordinate, "");
        jo = new JSONObject();
        try {
            jo.put("username", getUserid1);
            jo.put("mMonth","December");
            jo.put("mYear","2018");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                try {

                    Log.i("getting data of year",response.toString());
                    boolean result = response.getBoolean("result");
                    if(result){
                        String daycount = response.getString("Data");
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Dec, daycount); // Storing string
                        editor.commit();
                        decT.setText(sharedpreferences.getString(Dec,""));
                    }else{
                        Toast.makeText(getApplicationContext(),"data not " +
                                "found",Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"catch block of month",Toast.LENGTH_LONG).show();
                    //                            simpleProgressBar.setVisibility(View.INVISIBLE);

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            //                            Log.d(TAG, error.toString());
                            //Toast.makeText(getActivity(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
                            //
                        }
                    }
                }
        );
        requestQueue.add(jsonObjReq);
    }


}
