package com.zeslab.attendance;

/**
 * Created by kumar on 26-12-2017.
 */

public class CatoData {
    private String text;
    private String mobile;
    private String email;
    private String password;

    public CatoData(String text, String mobile, String email, String password) {
        this.text = text;
        this.mobile = mobile;
        this.email = email;
        this.password = password;

    }
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassowrd(String password) { this.password = password; }

}
