package com.zeslab.attendance;

/**
 * Created by kumar on 17-04-2018.
 */

public class AttendenceData {
    private String hours;
    private String date;

    public AttendenceData(String date, String hours) {
        this.date = date;
        this.hours = hours;


    }
    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
