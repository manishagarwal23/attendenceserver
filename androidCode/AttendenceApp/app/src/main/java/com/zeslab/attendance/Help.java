package com.zeslab.attendance;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

public class Help extends AppCompatActivity {

    TextView title;
    ImageView img;
    String startValue="2";
    TextView skip;
    VideoView videoDEmo;
    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Intro = "intro";
    public static final String IsLogin = "islogin";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help);
        img = (ImageView) findViewById(R.id.rightmenu);
        title = (TextView) findViewById(R.id.title);
        img.setVisibility(View.GONE);
        title.setText("Introduction");
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference, 0); // 0 - for private mode
        editor = sharedpreferences.edit();
        skip = (TextView) findViewById(R.id.skip_text);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoDEmo.stopPlayback();
                editor.putString(Intro, "1"); // Storing string
                editor.commit();
                Intent i = new Intent(Help.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });
        startValue = sharedpreferences.getString(Intro,"");
        if(startValue.equals("1")){

            Intent i = new Intent(Help.this, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
            editor.putString(Intro, "1"); // Storing string
            editor.commit();
        }
        videoDEmo = (VideoView) findViewById(R.id.videoView);
        videoDEmo.setVideoPath("android.resource://" + getPackageName() + "/" + R.raw.help);
        MediaController mediacontroller = new MediaController(this);
        mediacontroller.setAnchorView(videoDEmo);
        videoDEmo.setMediaController(mediacontroller);

        boolean login = sharedpreferences.getBoolean(IsLogin,false);
        if(!login){
            buildAlertMessageNoGps();
        }else {
            videoDEmo.start();
        }

    }
    private void buildAlertMessageNoGps() {
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference, 0); // 0 - for private mode
        editor = sharedpreferences.edit();
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to play Introduction video? The video is also present in the help section of the app for reference.")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        videoDEmo.start();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        editor.putString(Intro, "1"); // Storing string
                        editor.commit();
                        Intent i = new Intent(Help.this, MainActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
