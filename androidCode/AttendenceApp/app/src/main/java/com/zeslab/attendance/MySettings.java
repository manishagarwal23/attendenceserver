package com.zeslab.attendance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MySettings extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    TextView Update_groupname,title,show_my_subordinate,set_my_subordinate,
            pay_now,set_my_manager,share_to_manager,update_office_location,refund_now,
            our_products;
    Switch s;
    ImageView img;
    String getusername;
    String groupname;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Automatic = "auto";
    public static final String Automatictext = "autotext";
    public static final String Username = "username";
    public static final String Officebtnvorg = "setofficebtn";
    public static final String Groupname = "groupname";
    public static final String Actiontoperform = "actiontoperform";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_settings);
        pay_now                 = (TextView) findViewById(R.id.pay_now);
        show_my_subordinate     = (TextView) findViewById(R.id.show_my_subordinate);
        set_my_subordinate      = (TextView) findViewById(R.id.set_my_subordinate);
        set_my_manager          = (TextView) findViewById(R.id.set_my_manager);
        update_office_location  = (TextView)   findViewById(R.id.update_office_location);
        Update_groupname        = (TextView) findViewById(R.id.Update_groupname);
        share_to_manager        = (TextView) findViewById(R.id.share_to_manager);
        refund_now              = (TextView) findViewById(R.id.refund_now);
        our_products              = (TextView) findViewById(R.id.products);

        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        getusername = sharedpreferences.getString(Username,"");
        groupname = sharedpreferences.getString(Groupname,"");
        boolean auto = sharedpreferences.getBoolean(Automatic,false);


        update_office_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String checkbutton = sharedpreferences.getString(Officebtnvorg,"");
                if(checkbutton.equals("GONE")){
                    sharedpreferences.edit().putString(Officebtnvorg,"VISIBLE").apply();
                    utlitis.getInstance().set_Location.setVisibility(View.VISIBLE);
                    Intent i = new Intent(MySettings.this,MyMaps.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                }

            }
        });
        Update_groupname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MySettings.this, Groupname.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });
        set_my_subordinate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent subordinate = new Intent(MySettings.this, AdduserAsSubordinate.class );
                subordinate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(subordinate);
            }
        });
        show_my_subordinate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent imanager = new Intent(MySettings.this, ManagerActivity.class );
                 imanager.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                 startActivity(imanager);
            }
        });
        set_my_manager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent subordinate = new Intent(MySettings.this, AddMyManager.class );
                subordinate.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(subordinate);

            }
        });
        share_to_manager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "Kindly add me as a manager " +
                        " in your attendance app my username is "+getusername+" and groupname is "+groupname);
                startActivity(Intent.createChooser(sharingIntent, "Share your username with your Subordinates"));
            }
        });
        img = (ImageView) findViewById(R.id.backmenu);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MySettings.this,MyMaps.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        our_products .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MySettings.this,Products.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
        refund_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MySettings.this,Refund_Payment.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
        pay_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MySettings.this,PaymentDetail.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
        title = findViewById(R.id.title);
        title.setText("My Settings");
        s = (Switch) findViewById(R.id.automaticgps);
        s.setChecked(auto);
        if (s != null) {
            s.setOnCheckedChangeListener(this);
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//        Toast.makeText(this, "The Switch is " + (isChecked ? "on" : "off"),
//                Toast.LENGTH_SHORT).show();
        if(isChecked) {
            //do stuff when Switch is ON

            sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                    Context.MODE_PRIVATE);
            sharedpreferences.edit().putString(Actiontoperform, "IN").commit();
            sharedpreferences.edit().putString(Automatictext,"automode: you are outside the office").commit();
            sharedpreferences.edit().putBoolean(Automatic,isChecked).apply();
            utlitis.getInstance().in_button.setVisibility(View.GONE);
            utlitis.getInstance().auto.setVisibility(View.VISIBLE);


        } else {
            //do stuff when Switch if OFF
            sharedpreferences.edit().putBoolean(Automatic,isChecked).apply();
            utlitis.getInstance().in_button.setVisibility(View.VISIBLE);
            utlitis.getInstance().auto.setVisibility(View.GONE);
        }
        boolean getvalue = sharedpreferences.getBoolean(Automatic,false);
        Toast.makeText(this, "Automatic mode is " + (getvalue ? "on" : "off"),
                Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "Location will updated after 5 minutes later",
                Toast.LENGTH_SHORT).show();
    }
}
