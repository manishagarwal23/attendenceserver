package com.zeslab.attendance;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Idea_for_development extends AppCompatActivity {

    Button idea_button;
    ImageView back_to_main;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.idea_for_development);
        idea_button = (Button)   findViewById(R.id.idea_button);
        idea_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentEmail = new Intent(Intent.ACTION_SEND);
                intentEmail.setType("message/rfc822");
                intentEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{"ekta@zeslab.com"});

                intentEmail.putExtra(Intent.EXTRA_SUBJECT, "");

                intentEmail.putExtra(Intent.EXTRA_TEXT, "");

                startActivity(Intent.createChooser(intentEmail, "Send Email"));
            }
        });

        ////////////////     back to main   //////////////
        back_to_main = (ImageView) findViewById(R.id.back_to_main);
        back_to_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Idea_for_development.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

    }
}
