package com.zeslab.attendance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ManagerActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    private List<UserData> mDataset;
    private ManagerRecyclerviewAdapter adapter;
    TextView title;
    ImageView img;
    public ProgressBar progressBar;
    TextView resulttext;
    Button refresh;
    TextView nointernet;

    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Userid = "userid";
    public static final String Ismanagerwatching = "ismanagerwatching";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Ismanagerwatching, "manager"); // Storing string
        editor.commit();
        String imw = sharedpreferences.getString(Ismanagerwatching,"");
        resulttext = (TextView) findViewById(R.id.resulttext);

//        Log.i("ismanagerwatching manag",imw);
//        Toast.makeText(getApplicationContext(), imw +"::manager" ,Toast.LENGTH_LONG).show();

        progressBar = (ProgressBar) findViewById(R.id.progressBar3);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mDataset  = new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new ManagerRecyclerviewAdapter(getApplicationContext(), mDataset);
        recyclerView.setAdapter(adapter);
        img = (ImageView) findViewById(R.id.backmenu);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ManagerActivity.this,MySettings.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
        title = (TextView) findViewById(R.id.title);

        title.setText("Your Subordinates");
        //////////////////// CHECKING INTERNET CONNECTIONS ////////////////////////////
        refresh = (Button)findViewById(R.id.refresh);
        nointernet =(TextView)findViewById(R.id.nointernet);
        ConnectivityManager connMgr = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if( networkInfo == null ){
            nointernet.setText("You are not connected to internet");
            refresh.setVisibility(View.VISIBLE);
            nointernet.setVisibility(View.VISIBLE);
        }
        show_users_from_server();
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show_users_from_server();
                refresh.setVisibility(View.INVISIBLE);
                nointernet.setVisibility(View.INVISIBLE);
            }
        });
        //////////////////// CHECKING INTERNET CONNECTIONS ////////////////////////////
    }
    private void show_users_from_server() {

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        progressBar.setVisibility(View.VISIBLE);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/users/manager";
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        String userid =  sharedpreferences.getString(Userid,"");
        JSONObject jo = new JSONObject();
        try {
            jo.put("_id",userid);
            Log.i("userid of manager",userid);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                url,jo,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                Log.i("mangerActivity",response.toString());
                            mDataset.clear();
                            adapter.notifyDataSetChanged();

                            JSONArray jsonArray = response.getJSONArray("Data");
//                            ArrayList arrayList = response.getJSONArray("Data");
                            if(jsonArray.length()>=1){


                            for (int i = 0; i < jsonArray.length() ; i++) {
//                                JSONObject jsonobject = jsonArray.getJSONObject(i);
                                String name = jsonArray.getString(i);


                                UserData data = new UserData(name);

                                mDataset.add(data);
                                adapter.notifyDataSetChanged();
                                progressBar.setVisibility(View.GONE);
                                resulttext.setVisibility(View.GONE);

                            }
                            }else {
                                resulttext.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            progressBar.setVisibility(View.GONE);

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            Log.d("volley error", error.toString());
//                            Toast.makeText(getApplicationContext(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
                            progressBar.setVisibility(View.GONE);
                            nointernet.setText("Check your internet connections");
                            refresh.setVisibility(View.VISIBLE);
                            nointernet.setVisibility(View.VISIBLE);
                        }
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }
}
