package com.zeslab.attendance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class PaymentDetail extends AppCompatActivity {

    ImageView backmenu;
    Button pay_now_final;
    EditText amount,phone,email,fullname,groupname;
    String amountS,phoneS,emailS,fullnameS,ProductInfoS,groupnameS;
    String finalamount;
    TextView title,ProductInfo;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Username = "username";
    public static final String Groupname = "groupname";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_detail);

        title         =  (TextView)  findViewById(R.id.title);
        backmenu      =  (ImageView) findViewById(R.id.backmenu);
        pay_now_final =  (Button)    findViewById(R.id.pay_now_final);
        fullname      =  (EditText)  findViewById(R.id.fullname);
        email         =  (EditText)  findViewById(R.id.email);
        phone         =  (EditText)  findViewById(R.id.phone);
        amount        =  (EditText)  findViewById(R.id.amount);
        groupname     =  (EditText)  findViewById(R.id.groupname);
        fullname.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        groupname.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        fullnameS = sharedpreferences.getString(Username,"");
        groupnameS = sharedpreferences.getString(Groupname,"");
        fullname.setText(fullnameS);
        groupname.setText(groupnameS);
        ProductInfoS = "Username:"+fullname+" groupname:"+groupnameS;

        title.setText("Payment Details");
        Intent i = getIntent();
        finalamount = i.getExtras().getString("amount");
        amount.setText(finalamount);


        backmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PaymentDetail.this,MySettings.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });

        /////////////////////   MAKE PAYMENY FINALLY  ///////////////////
        pay_now_final.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fullnameS = fullname.getText().toString();
                emailS = email.getText().toString();
                phoneS = phone.getText().toString();
                amountS = amount.getText().toString();
//                ProductInfoS = ProductInfo.getText().toString();
                groupnameS = groupname.getText().toString();
                double amt = Double.parseDouble(amountS);
                if(fullnameS.length()>2 && emailS.length()>0 && isValidEmail(emailS) && phoneS.length()==10 && amt>0 && groupnameS.length()>1){

                    Intent i = new Intent(PaymentDetail.this,PayUMoneyActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.putExtra("name",fullnameS);
                    i.putExtra("email",emailS);
                    i.putExtra("amount",amt);
                    i.putExtra("phone",phoneS);
                    i.putExtra("ProductInfo",ProductInfoS);
                    i.putExtra("groupname",groupnameS);
                    startActivity(i);

                }else {
                    Toast.makeText(getApplicationContext(),"please enter " +
                            "valid details",Toast.LENGTH_SHORT).show();
                }
            }
        });
        /////////////////////   MAKE PAYMENY FINALLY  ///////////////////


    }
    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
