package com.zeslab.attendance;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UsersDetails extends AppCompatActivity {
    private List<CatoData> mDataset;
    private static RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private CategoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_details);
        mDataset  = new ArrayList<>();
        recyclerView =findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);

        adapter = new CategoryAdapter(this.getApplicationContext(), mDataset);
        recyclerView.setAdapter(adapter);
        load_data_from_server_for_usersDatabase();
    }

    private void load_data_from_server_for_usersDatabase() {

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/users";
//        simpleProgressBar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            // Create the root JSONObject from the JSON string.

//                            JSONObject jsonRootObject = new JSONObject(response);
                            //Get the instance of JSONArray that contains JSONObjects
                            mDataset.clear();
                            adapter.notifyDataSetChanged();
                            JSONArray jsonArray =  new JSONArray(response);
                            for (int i = 0; i < jsonArray.length() ; i++) {
                                JSONObject jsonobject = jsonArray.getJSONObject(i);
                                String name = jsonobject.getString("name");
                                String mobile = jsonobject.getString("mobile");
                                String email = jsonobject.getString("email");
                                String password = jsonobject.getString("password");
                                CatoData data = new CatoData(name, mobile, email,password);
//
                                mDataset.add(data);
                                adapter.notifyDataSetChanged();
////
//                                refresh.setVisibility(View.INVISIBLE);
//                                nointernet.setVisibility(View.INVISIBLE);
                            }
//                            adapter.setLoaded();
                        } catch (JSONException e) {
                            e.printStackTrace();
//                            simpleProgressBar.setVisibility(View.INVISIBLE);

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
//                            Log.d(TAG, error.toString());
                            //Toast.makeText(getActivity(), "Oops... Check your internet connection", Toast.LENGTH_LONG).show();
//                            simpleProgressBar.setVisibility(View.INVISIBLE);
//                            nointernet.setText("Check your internet connections");
//                            refresh.setVisibility(View.VISIBLE);
//                            nointernet.setVisibility(View.VISIBLE);

                        }
                    }
                }
        );
        requestQueue.add(stringRequest);
    }
}
