package com.zeslab.attendance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

public class Refund_Payment extends AppCompatActivity {

    Button refund_now;
    TextView paymentdetailtext;
    ImageView backmenu;
    String respn,getEmail,getGroupname;
    TextView title;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Email = "email";
    public static final String Groupname = "groupname";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refund__payment);
        title = (TextView) findViewById(R.id.title);
        refund_now  =(Button) findViewById(R.id.refund_now);
        paymentdetailtext = (TextView) findViewById(R.id.paymentdetailtext);
        backmenu = (ImageView) findViewById(R.id.backmenu);
//        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
//                Context.MODE_PRIVATE);
//        getEmail = sharedpreferences.getString(Email,"");
//        getGroupname = sharedpreferences.getString(Groupname,"");
//        load_transaction_detail();
        title.setText("Refund");
        backmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Refund_Payment.this,MySettings.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });
        refund_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentEmail = new Intent(Intent.ACTION_SEND);
                intentEmail.setType("message/rfc822");
                intentEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{"ekta@zeslab.com"});
                intentEmail.putExtra(Intent.EXTRA_CC, new String[]{"bhaskar@zeslab.com"});
                intentEmail.putExtra(Intent.EXTRA_SUBJECT, "");
                intentEmail.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(intentEmail, "Send Email"));
            }
        });
     }

    public void load_transaction_detail(){
        RequestQueue reqC = Volley.newRequestQueue(this);

        String ip_port = getString(R.string.url);
        String url = ip_port+"/refund/payment";


        JSONObject jo = new JSONObject();
        try {
            jo.put("groupname", getGroupname);
            jo.put("email", getEmail);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest str_req = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                /////////RESPONSE JSON STORE START////////
                respn = response.toString();
                Log.i("res1", String.valueOf(respn));

                try {
                    boolean result = response.getBoolean("result");
                    Log.i("booleanres", String.valueOf(result));
                    if(result){
                        JSONObject ji1 =  response.getJSONObject("Data");
                        String transactionid = ji1.getString("pTransaction");
                        String amount = ji1.getString("pAmount");
                        String groupname = ji1.getString("pGroupname");
                        String phone = ji1.getString("pPhone");



                    }else{
                        Toast.makeText(getApplicationContext(),
                                "load failed", Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();


                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("volleyerror",error.toString());

                        Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_LONG).show();
                    }


                });

        // add json request in volley request queue
        reqC.add(str_req);

        //Toast.makeText(getApplicationContext(), "Thank You !!", Toast.LENGTH_LONG).show();

    }
}
