package com.zeslab.attendance;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.SystemClock;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.Resource;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Bee on 4/9/2018.
 *
 */
// Java program implementing utlitis class
// with getInstance() method
class utlitis
{
    Toolbar toolbar;
    String actiontoperform;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Userid = "userid";
    public static final String Username = "username";
    public static final String Officerange = "officerange";
    public static final String Loginornot = "loginornot";
    public static final String IsLogin = "islogin";
    public static final String Officelat = "Officelat";
    public static final String Officelong = "Officelong";
    public static final String Userlat = "userlat";
    public static final String Userlong = "userlong";
    public static final String Actiontoperform = "actiontoperform";
    public static final String Settingattendancebuttonvalue = "settingattendancebuttonvalue";
    public static final String Settingfirsttimeattendancevalue = "settingfirsttimeattendancevalue";
    public static final String Officebtnvorg = "setofficebtn";
    public static final String Automatic = "auto";
    public static final String Automatictext = "autotext";
    public static final String Currentdate = "currentdate";
    Context context,adscontext;
    private GoogleMap mMap;
    LocationManager locationManager;
    LocationListener locationListener;
    //    Location location;
    String provider;
    String in_Date;
    Double lat, lan;
    Button in, out;
    int mYear, mMonth, mDate, out_Date, in_Hours, in_Minutes, out_Minutes, out_Hours;
    JSONObject jo, ji;
    ImageView rightMenu;
    Button in_button, auto;
    utlitis util;
    TextView set_Location;
    static final String Atcid = "atcid";
    String Id, getId, getUserId;
    String ATW;
    String respn;
    LatLng user_location, lastlocation;
    NavigationView navigationView;
    DrawerLayout drawer;
    Calendar calendar;
    SimpleDateFormat format;
    TextView userNameTitle,distance;
    double latitude = 0.0, longitude = 0.0;
    LocationTracker locationTracker;
    public MyMaps maps;
    public GoogleMap googleMap;
    // static variable single_instance of type utlitis
    private static utlitis single_instance = null;
    boolean updatedLocation= false;
    /// ads processing bar
    public ProgressBar progressBar;
    ImageView adsimage;
    // variable of type String
    public String s;
    // flag for one time location update
    boolean isLocationUpdated = true;
    CheckBox yearly_report,group_payment;

    // private constructor restricted to this class itself
    private utlitis()
    {
        s = "Hello I am a string part of utlitis class";
    }

    // static method to create instance of utlitis class
    public static utlitis getInstance()
    {
        if (single_instance == null)
            single_instance = new utlitis();

        return single_instance;
    }
    public void making_JSONObject(){
        String timezone = TimeZone.getDefault().getID();
        calendar = Calendar.getInstance();
        format = new SimpleDateFormat("dd/MM/yyyy");
        sharedpreferences = context.getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        actiontoperform = sharedpreferences.getString(Actiontoperform,"");

        mYear = Calendar.getInstance().get(Calendar.YEAR);
        mMonth = Calendar.getInstance().get(Calendar.MONTH);
        mDate = Calendar.getInstance().get(Calendar.DATE);
        out_Date = Calendar.getInstance().get(Calendar.DATE);
        out_Hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        out_Minutes = Calendar.getInstance().get(Calendar.MINUTE);
        in_Date = format.format(calendar.getTime());
        in_Hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        in_Minutes = Calendar.getInstance().get(Calendar.MINUTE);
        getUserId = sharedpreferences.getString(Userid, "");
        String   getUsername =  sharedpreferences.getString(Username,"");
        getId = sharedpreferences.getString(Atcid, "");
        sharedpreferences.edit().putInt(Currentdate,mDate).commit();

//                    getOfficelocation = sharedpreferences.getString(Officelocation,"");
//                    Log.i("officelocation",getOfficelocation+"is office locaion");


        jo = new JSONObject();
        try {
            jo.put("id", getId);
            jo.put("User_Id", getUserId);
            jo.put("username", getUsername);
            jo.put("mYear", mYear);
            jo.put("mMonth", mMonth);
            jo.put("mDate", mDate);
            jo.put("in_Date", in_Date);
            jo.put("in_Hour", in_Hours);
            jo.put("in_Minute", in_Minutes);
            jo.put("out_Date", out_Date);
            jo.put("out_Hour",out_Hours);
            jo.put("out_Minute",out_Minutes);
            jo.put("timezone",timezone);
            jo.put("Location_on_InTime", user_location);
            jo.put("Location_on_OutTime",user_location);
            jo.put("officeLat", user_location.latitude);
            jo.put("officeLong", user_location.longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(actiontoperform.equals("IN")) {
            try {
                jo.put("action_to_perform", "InTime");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(actiontoperform.equals("OUT")){
            try {
                jo.put("id", getId);
                jo.put("action_to_perform", "OutTime");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        insert_attendence_into_database();
        Log.i("log of jo object",jo.toString());

    }

    public void insert_attendence_into_database() {
        RequestQueue reqC = Volley.newRequestQueue(context);
        String ip_port = context.getString(R.string.url);
        String url = ip_port + "/attendence";
        sharedpreferences = context.getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        in_button.setText("Working...");
        JsonObjectRequest str_req = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                /////////RESPONSE JSON STORE START
                respn = response.toString();
                Log.i("user location server", String.valueOf(respn));
                try {
                    boolean result = response.getBoolean("result");
                    boolean nearoffice = response.getBoolean("nearoffice");
                    if(result){
                        if(nearoffice){
                            savingId();
                        }else {
                            in_button.setText(sharedpreferences.getString(Settingattendancebuttonvalue, ""));
                            in_button.setEnabled(true);
                            Toast.makeText(context, "You are not near to office", Toast.LENGTH_LONG).show();
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("volley error", error.toString());
                        in_button.setText(sharedpreferences.getString(Settingattendancebuttonvalue, ""));
                        in_button.setEnabled(true);
                        Toast.makeText(context, "Check your Internet connection", Toast.LENGTH_LONG).show();
                    }

                });

        // add json request in volley request queue
        reqC.add(str_req);

        //Toast.makeText(getApplicationContext(), "Thank You !!", Toast.LENGTH_LONG).show();

    }

    public void savingId() {
        try {
            ji = new JSONObject(respn);
            JSONObject ji1 =  ji.getJSONObject("Data");
            Id = ji1.getString("_id");
            ATW = ji1.getString("action_to_perform");
            sharedpreferences = context.getSharedPreferences(mypreference,
                    Context.MODE_PRIVATE);
            if (ATW.equals("InTime")) {
                sharedpreferences.edit().putString(Actiontoperform, "OUT").commit();
                String atf = sharedpreferences.getString(Actiontoperform, "");
                Log.i("atf_out", atf);
                sharedpreferences.edit().putString(Settingattendancebuttonvalue, "OUT").commit();
                in_button.setText(sharedpreferences.getString(Settingattendancebuttonvalue, ""));
                in_button.setEnabled(true);
                sharedpreferences.edit().putString(Automatictext,"automode: you are in the office").commit();
                auto.setText(sharedpreferences.getString(Automatictext,""));
            } else if (ATW.equals("OutTime")) {
                sharedpreferences.edit().putString(Actiontoperform, "IN").commit();
                String atf = sharedpreferences.getString(Actiontoperform, "");
                Log.i("atf_in", atf);
                sharedpreferences.edit().putString(Settingattendancebuttonvalue, "IN").commit();
                in_button.setText(sharedpreferences.getString(Settingattendancebuttonvalue, ""));
                in_button.setEnabled(true);
                sharedpreferences.edit().putString(Automatictext,"automode: you are outside the office").commit();
                auto.setText(sharedpreferences.getString(Automatictext,""));
            }
//            Toast.makeText(context, ATW, Toast.LENGTH_LONG).show();

            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(Atcid, Id); // Storing string
            editor.commit();
            String show = sharedpreferences.getString(Atcid, "");
            Log.i("show", show);
//            }

        } catch (JSONException e) {
            e.printStackTrace();
            in_button.setText(sharedpreferences.getString(Settingattendancebuttonvalue, ""));
            in_button.setEnabled(true);
        }
    }



}


