package com.zeslab.attendance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

public class AdsActivity extends AppCompatActivity {
    ImageView adsimage;
    Context adscontext;
    public ProgressBar progressBar;
    boolean adsloded = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads);

        adscontext = getApplicationContext();
        utlitis.getInstance().adscontext = adscontext;
        progressBar = findViewById(R.id.progressBar4);
        utlitis.getInstance().progressBar = progressBar;
        adsimage = (ImageView) findViewById(R.id.adsimage);
        utlitis.getInstance().adsimage = adsimage;
        Bundle extras = getIntent().getExtras();
        Bitmap bmp = (Bitmap) extras.getParcelable("imagebitmap");
        adsimage.setImageBitmap(bmp);

    }


    @Override
    public void onBackPressed() {

        final Thread myThread = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(900);

                        finish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        };
        myThread.start();

    }

}
