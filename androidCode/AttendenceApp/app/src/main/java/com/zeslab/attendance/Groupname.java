package com.zeslab.attendance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class Groupname extends AppCompatActivity {
    Button creategroup,joingroup;
    TextView skip;
    ImageView backmenu;
    EditText createtext,jointext;
    JSONObject jo,ji;
    String username,url,ip_port,respn;
    public ProgressBar progressBar;
    TextView userNameTitle;

    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Userid = "userid";
    public static final String Username = "username";
    public static final String Groupname = "groupname";
    public static final String IsLogin = "islogin";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groupname);
        ip_port = getString(R.string.url);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        progressBar = findViewById(R.id.progressBar0);
        backmenu = (ImageView) findViewById(R.id.backmenu);
        backmenu.setVisibility(View.GONE);
        RadioButton r1 = (RadioButton) findViewById(R.id.radio1);
        RadioButton r2 = (RadioButton) findViewById(R.id.radio2);
        jointext = (EditText) findViewById(R.id.jointext);
        jointext.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        creategroup = (Button) findViewById(R.id.createbtn);
        joingroup = (Button) findViewById(R.id.joingroup);
        createtext = (EditText) findViewById(R.id.newgrouptext);
        skip = (TextView) findViewById(R.id.skip);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               mtdisLogin();
                intent_to_maps();
            }
        });
        createtext.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        userNameTitle = (TextView) findViewById(R.id.title) ;
        username =  sharedpreferences.getString(Username,"");
        userNameTitle.setText(username);

        r2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jointext.setVisibility(View.GONE);
                joingroup.setVisibility(View.GONE);
                createtext.setVisibility(View.VISIBLE);
                creategroup.setVisibility(View.VISIBLE);
            }
        });
        r1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createtext.setVisibility(View.GONE);
                creategroup.setVisibility(View.GONE);
                joingroup.setVisibility(View.VISIBLE);
                jointext.setVisibility(View.VISIBLE);
            }
        });

        creategroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String gname = createtext.getText().toString();
                if(gname.length()>1){


                jo = new JSONObject();
                try {
                    jo.put("username",username);
                    jo.put("groupName",gname);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                url = ip_port+"/creategroup";
                save_my_groupname();
                }else {
                    Toast.makeText(getApplicationContext(),
                            "Groupname is small", Toast.LENGTH_LONG).show();
                }
            }
        });
        joingroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String gname = jointext.getText().toString();
                jo = new JSONObject();
                try {
                    jo.put("username",username);
                    jo.put("groupName",gname);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                url = ip_port+"/joingroup";
                save_my_groupname();
            }
        });
    }

    public void save_my_groupname(){
        RequestQueue reqC = Volley.newRequestQueue(this);

        progressBar.setVisibility(View.VISIBLE);

        JsonObjectRequest str_req = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                /////////RESPONSE JSON STORE START////////
                respn = response.toString();
                Log.i("save my groupname", String.valueOf(respn));
                try {
                    boolean result = response.getBoolean("result");
                    Log.i("booleanres", String.valueOf(result));
                    if(result){
                        savingId();
                    }else{

                        Toast.makeText(getApplicationContext(),
                                "Invalid groupname", Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.INVISIBLE);
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_LONG).show();
                    }


                });

        // add json request in volley request queue
        reqC.add(str_req);

        //Toast.makeText(getApplicationContext(), "Thank You !!", Toast.LENGTH_LONG).show();

    }

    public void savingId(){
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        int roleId = 0;
        try {
            ji = new JSONObject(respn);
            JSONObject ji1 =  ji.getJSONObject("Data");
            String username = ji1.getString("username");
            String role = ji1.getString("roleId");
            String groupname = ji1.getString("groupName");
            roleId = Integer.parseInt(role);

//                Toast.makeText(getApplicationContext(),ATW ,Toast.LENGTH_LONG).show();
            sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                    Context.MODE_PRIVATE);
            sharedpreferences.edit().putString(Groupname,groupname).apply();
//                   //////   have to make true so that user intent to mymaps activity
            String show1 = sharedpreferences.getString(Userid,"");
            Log.i("show",show1);

        } catch (JSONException e) {
            e.printStackTrace();
            progressBar.setVisibility(View.INVISIBLE);
            Log.i("show",e.toString());
            Toast.makeText(getApplicationContext(),"error while saving id",
                    Toast.LENGTH_LONG).show();

        }

        mtdisLogin();
        intent_to_payment_alert();
//        if(roleId == 2) {
//            intent_to_payment_alert();
//            String getusername = sharedpreferences.getString(Username,"");
//            String groupname = sharedpreferences.getString(Groupname,"");
//            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
//            sharingIntent.setType("text/plain");
//            sharingIntent.putExtra(Intent.EXTRA_TEXT, "Kindly add me as a manager " +
//                    " in your attendance app my username is "+getusername+" and groupname is "+groupname);
//            startActivity(Intent.createChooser(sharingIntent, "Share your username with your Subordinates"));
//        }else {
//            intent_to_maps();
//
//        }

    }
    public void intent_to_maps(){
        Intent i1 = new Intent(Groupname.this, MyMaps.class);
        i1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i1);
        finish();
    }
    public void intent_to_payment_alert(){
        Intent i1 = new Intent(Groupname.this, PaymentAlert.class);
        i1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i1);
        finish();
    }

    public void mtdisLogin(){
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(IsLogin, true); // Storing string
        editor.commit();
        progressBar.setVisibility(View.INVISIBLE);

    }

}
