package com.zeslab.attendance;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

public class Office extends AppCompatActivity {

    Button in,out;
    Date inTime,outTime;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    static final String Atcid = "atcid";
    JSONObject jo,ji;
    String Id,getId;
    String ATW;
    String respn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_office);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);

        in = findViewById(R.id.office_in);
        in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                inTime = Calendar.getInstance().getTime();
//                Toast.makeText(getApplicationContext(),inTime.toString(),Toast.LENGTH_LONG).show();
                 jo = new JSONObject();
                try {
                    jo.put("in_Time", inTime);
                    jo.put("action_to_perform","InTime");
                    Log.i("joData",jo.toString()+"null");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                insert_attendence_into_database();
            }
        });

        out = findViewById(R.id.office_out);
        out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                outTime = Calendar.getInstance().getTime();

                getId = sharedpreferences.getString(Atcid, "");
                Toast.makeText(getApplicationContext(),getId ,Toast.LENGTH_LONG).show();
                jo = new JSONObject();
                try {
                    jo.put("out_Time", outTime);
                    jo.put("id",getId);
                    jo.put("action_to_perform","OutTime");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                insert_attendence_into_database();
            }
        });
    }
    public void insert_attendence_into_database(){
        RequestQueue reqC = Volley.newRequestQueue(this);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/attendence";
        JsonObjectRequest str_req = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                /////////RESPONSE JSON STORE START////////
                respn = response.toString();
                Log.i("res", String.valueOf(respn));
                savingId();

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_LONG).show();
                    }


                });



        // add json request in volley request queue
        reqC.add(str_req);

        //Toast.makeText(getApplicationContext(), "Thank You !!", Toast.LENGTH_LONG).show();

    }


    public void savingId(){
        try {
            ji = new JSONObject(respn);
            Id = ji.getString("_id");
             ATW = ji.getString("action_to_perform");
            if(ATW.equals("InTime")){
                Toast.makeText(getApplicationContext(),ATW ,Toast.LENGTH_LONG).show();
                sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(Atcid, Id); // Storing string
                editor.commit();
                String show = sharedpreferences.getString(Atcid,"");
                Log.i("show",show);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
