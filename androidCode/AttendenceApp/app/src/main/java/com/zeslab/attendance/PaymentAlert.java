package com.zeslab.attendance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class PaymentAlert extends AppCompatActivity {

    Button proceed_to_pay,nextbtn;
    TextView skip,qgn,textalert;
    EditText egm;
    String getNumber,finalAmount;
    int mAmount;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Groupname = "groupname";
    public static final String Username = "username";
    public static final String Yreport = "yreport";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_alert);
        skip = (TextView) findViewById(R.id.skip_payment);
        egm = (EditText) findViewById(R.id.egm);
        qgn = (TextView) findViewById(R.id.qgn);
        nextbtn = (Button) findViewById(R.id.nextbtn);
        textalert = (TextView) findViewById(R.id.textalert);


        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getNumber = egm.getText().toString();

                if(!getNumber.equals("") ){
                    int amt = Integer.parseInt(getNumber);
                    if(amt>0){

                    mAmount = amt * 1000;
                    finalAmount = String.valueOf(mAmount);
//                    if(mAmount<6000){
//                        textalert.setText("You don't have to pay for "+amt+" members");
                    if(utlitis.getInstance().yearly_report != null){
                        if(utlitis.getInstance().yearly_report.isChecked()){
                            mAmount = mAmount + 1000;
                            finalAmount = String.valueOf(mAmount);

                        }
                    }

                        textalert.setVisibility(View.VISIBLE);
//                    }else{
                        textalert.setText("You have to pay Rs."+finalAmount);
                        textalert.setVisibility(View.VISIBLE);
                        proceed_to_pay.setVisibility(View.VISIBLE);
//                    }

                    qgn.setVisibility(View.GONE);
                    egm.setVisibility(View.GONE);
                    nextbtn.setVisibility(View.GONE);
                    }else {
                        Toast.makeText(getApplicationContext(),"please enter" +
                                " valid number",Toast.LENGTH_SHORT).show();
                    }

                }else {
                    Toast.makeText(getApplicationContext(),"please enter" +
                            " valid number",Toast.LENGTH_SHORT).show();
                }
            }
        });
        if(utlitis.getInstance().group_payment != null){
            if(utlitis.getInstance().group_payment.isChecked()== false){
                egm.setText("0");
                egm.callOnClick();
            }
        }


        proceed_to_pay = (Button) findViewById(R.id.proceed_to_pay);
        proceed_to_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PaymentAlert.this,PaymentDetail.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("amount",finalAmount);
                startActivity(i);
                finish();
            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PaymentAlert.this,MyMaps.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });

    }
}
