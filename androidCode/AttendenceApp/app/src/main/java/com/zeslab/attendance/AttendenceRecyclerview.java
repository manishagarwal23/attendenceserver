package com.zeslab.attendance;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

/**
 * Created by kumar on 17-04-2018.
 */

public class AttendenceRecyclerview extends RecyclerView.Adapter<AttendenceRecyclerview.ViewHolder> {

    private Context context;
    private List<AttendenceData> cities;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Category = "category";


    public AttendenceRecyclerview(Context context, List<AttendenceData> cities)  {
        this.context = context;
        this.cities = cities;
    }

    @Override
    public AttendenceRecyclerview.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.attendence_cardview,parent,false);

        return new AttendenceRecyclerview.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AttendenceRecyclerview.ViewHolder holder, final int position) {
        holder.dates.setText(cities.get(position).getDate());
        holder.hours.setText(cities.get(position).getHours());

    }

    @Override
    public int getItemCount() {
        if(cities == null)
            return 0;
        return cities.size();
    }

    public  class ViewHolder extends  RecyclerView.ViewHolder {

        Button dates, hours;


        public ViewHolder(View itemView) {
            super(itemView);
           dates = itemView.findViewById(R.id.dates);
           hours = itemView.findViewById(R.id.hours);

 //            categorytext.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {

//                    String cid = cid_textview.getText().toString();
//                    sharedpreferences = context.getSharedPreferences(mypreference,
//                            Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editor = sharedpreferences.edit();
//                    editor.putString(Category,cid);
//                    editor.commit();
//                    Intent intent = new Intent(view.getContext(),MainActivity.class );
//
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    view.getContext().startActivity(intent);
//                    if(getActivity() != null) {
//                        viewPager.setCurrentItem(0); // FIRST fragment index is 0
////                                            Toast.makeText(getContext(), cato_id, Toast.LENGTH_SHORT).show();
////                                            Log.i("clicked ", " clicked on "+ categoryname);
//                    }

        }
//            });
//
//        }


    }
}