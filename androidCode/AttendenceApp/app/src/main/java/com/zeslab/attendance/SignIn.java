package com.zeslab.attendance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import static android.view.View.VISIBLE;

public class SignIn extends AppCompatActivity {
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Userid = "userid";
    public static final String Username = "username";
    public static final String IsLogin = "islogin";
    public static final String Signuporloginpage = "signuporloginpage";
    public static final String Groupname = "groupname";
    public ProgressBar progressBar;
    String Id;
    String respn;
    JSONObject ji;
    EditText username, password;
    TextView SignUp;
    Button login;
    String usernameS,passwordS;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        login    = (Button)   findViewById(R.id.login);
        progressBar = (ProgressBar) findViewById(R.id.progressBar2);
        username.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkUsername_and_Password(view);
            }
        });
        /////////   signup line that will switch to you signup page
        SignUp   = (TextView) findViewById(R.id.SignUp);
        SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(Signuporloginpage, "Signup"); // Storing string
                editor.commit();
                Intent i = new Intent(SignIn.this,MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();


            }
        });
    }
    public void checkUsername_and_Password(View view){
        usernameS = username.getText().toString();
        passwordS = password.getText().toString();
        if(usernameS.length()>2){
            if(passwordS.length()>3){
                login.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                login_through_database();
            }else{
                Toast.makeText(getApplicationContext(),
                        "use 4 length or more",Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(getApplicationContext(),
                    "too short name",Toast.LENGTH_LONG).show();
        }
    }
    public void mtdisLogin(){
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(IsLogin, true); // Storing string
        editor.commit();

        Intent i1 = new Intent(SignIn.this, MyMaps.class);
        i1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i1);
        finish();
        progressBar.setVisibility(View.INVISIBLE);
    }
    public void savingId(){
        try {
            ji = new JSONObject(respn);
            JSONObject ji1 =  ji.getJSONObject("Data");
            Id = ji1.getString("_id");
            String username = ji1.getString("username");
            String groupname =  ji1.getString("groupName");
//                Toast.makeText(getApplicationContext(),ATW ,Toast.LENGTH_LONG).show();
            sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(Userid, Id); // Storing string
            editor.putString(Username, username);
            editor.putString(Groupname,groupname);
            editor.commit();
            String show = sharedpreferences.getString(Userid,"");
            Log.i("logshow",show);
        } catch (JSONException e) {
            e.printStackTrace();

        }
        mtdisLogin();
    }

    public void login_through_database(){
        RequestQueue reqC = Volley.newRequestQueue(this);
        progressBar.setVisibility(View.VISIBLE);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/login";
        usernameS = username.getText().toString();
        passwordS = password.getText().toString();
        JSONObject jo = new JSONObject();
        try {
            jo.put("username", usernameS);
            jo.put("password", passwordS);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest str_req = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                /////////RESPONSE JSON STORE START////////
                respn = response.toString();
                Log.i("res1", String.valueOf(respn));

                try {
                    boolean result = response.getBoolean("result");
                    Log.i("booleanres", String.valueOf(result));
                    if(result){
                        savingId();
                    }else{
                        Toast.makeText(getApplicationContext(),
                                "login failed", Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.INVISIBLE);
                        login.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.INVISIBLE);
                    login.setVisibility(View.VISIBLE);
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                Log.i("volleyerror",error.toString());
                        progressBar.setVisibility(View.INVISIBLE);
                        login.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_LONG).show();
                    }


                });



        // add json request in volley request queue
        reqC.add(str_req);

        //Toast.makeText(getApplicationContext(), "Thank You !!", Toast.LENGTH_LONG).show();

    }
}
