package com.zeslab.attendance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Products extends AppCompatActivity {

    ImageView backmenu;
    TextView title;
    Button proceed_to_pay;
    CheckBox yearly_report,group_payment;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Groupname = "groupname";
    public static final String Username = "username";
    public static final String Yreport = "yreport";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        title = (TextView) findViewById(R.id.title);
        backmenu = (ImageView) findViewById(R.id.backmenu);
        title.setText("Our Products");

        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        group_payment = (CheckBox) findViewById(R.id.group_payment);


        yearly_report = (CheckBox) findViewById(R.id.yearly_report);
        proceed_to_pay = (Button)  findViewById(R.id.proceed_payment);
        utlitis.getInstance().group_payment = group_payment;
        utlitis.getInstance().yearly_report = yearly_report;

        proceed_to_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(group_payment.isChecked() || yearly_report.isChecked()){


                if(yearly_report.isChecked()){
                    sharedpreferences.edit().putBoolean(Yreport,true).commit();
                }
                Intent i = new Intent(Products.this,PaymentAlert.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                }else {
                    Toast.makeText(getApplicationContext(),"Please check atleast" +
                            " one product",Toast.LENGTH_LONG).show();
                }
            }
        });

//        group_payment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(Products.this,PaymentAlert.class);
//                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(i);
//            }
//        });

//        yearly_report.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//               sharedpreferences.edit().putBoolean(Yreport,true).commit();
//                Intent i = new Intent(Products.this,PaymentDetail.class);
//                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                i.putExtra("amount","1000");
//                startActivity(i);
//                finish();
//            }
//        });

        backmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Products.this,MySettings.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }

}
