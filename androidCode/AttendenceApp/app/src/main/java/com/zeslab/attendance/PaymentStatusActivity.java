package com.zeslab.attendance;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class PaymentStatusActivity extends AppCompatActivity {

    TextView payment_Detail;
    String mAmount,mPhone,mEmail,mTransactionId,mGroupname;
    public ProgressBar progressBar;
    boolean mStatus;
    double damount;
    String respn;
    FloatingActionButton okfb,errorfb;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Email = "email";
    public static final String Username = "username";
    public static final String Groupname = "groupname";
    public static final String Yreport = "yreport";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_status);
        progressBar = (ProgressBar) findViewById(R.id.progressBar3);
        payment_Detail = (TextView) findViewById(R.id.payment_Detail);
        okfb    =   (FloatingActionButton) findViewById(R.id.okfb);
        errorfb =    (FloatingActionButton) findViewById(R.id.errorfb);
        sharedpreferences = getApplicationContext().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        Intent i =  getIntent();
        mStatus = i.getExtras().getBoolean("status");
        mTransactionId = i.getExtras().getString("transaction_id");
        mEmail         = i.getExtras().getString("email");
        mPhone         = i.getExtras().getString("phone");
        damount        = i.getExtras().getDouble("amount");
        mGroupname     = i.getExtras().getString("groupname");
        mAmount = String.valueOf(damount);
        if(utlitis.getInstance().yearly_report.isChecked()){
            send_mail();
        }

    if(mTransactionId != null){

            if(mStatus == true){
                payment_Detail.setText("Your Payment"+mAmount+" is sucessfull \n " +
                        "and your transaction id is "+mTransactionId);
                sharedpreferences.edit().putString(Email,mEmail).commit();
            }else {
                payment_Detail.setText("Your Payment"+mAmount+" is failed \n " +
                        "and your transaction id is "+mTransactionId);
            }
        }


        login_through_database();
    }
    public void login_through_database(){
        RequestQueue reqC = Volley.newRequestQueue(this);
        progressBar.setVisibility(View.VISIBLE);
        String ip_port = getString(R.string.url);
        String url = ip_port+"/paymentTransaction";


        Log.i("data: ","amount"+mAmount+"pnone"+mPhone+"email"+mEmail);
        JSONObject jo = new JSONObject();
        try {
            jo.put("status", mStatus);
            jo.put("transaction_id", mTransactionId);
            jo.put("email", mEmail);
            jo.put("phone", mPhone);
            jo.put("amount", mAmount);
            jo.put("groupname", mGroupname);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest str_req = new JsonObjectRequest(Request.Method.POST,
                url, jo, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                /////////RESPONSE JSON STORE START////////
                respn = response.toString();
                Log.i("res1", String.valueOf(respn));

                try {
                    boolean result = response.getBoolean("result");
                    Log.i("booleanres", String.valueOf(result));
                    if(result){
                        boolean b1 = response.getBoolean("Data");
                        if(b1){
                            progressBar.setVisibility(View.GONE);
                            okfb.setVisibility(View.VISIBLE);

                        }
                    }else{
                        Toast.makeText(getApplicationContext(),
                                "login failed", Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.INVISIBLE);
                        errorfb.setVisibility(View.VISIBLE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.INVISIBLE);

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("volleyerror",error.toString());
                        progressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_LONG).show();
                    }


                });

        // add json request in volley request queue
        reqC.add(str_req);

        //Toast.makeText(getApplicationContext(), "Thank You !!", Toast.LENGTH_LONG).show();

    }
    @Override
    public void onBackPressed() {
        onPressingBack();
    }

    private void onPressingBack() {
        Intent i = new Intent(PaymentStatusActivity.this,MySettings.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }
    public void send_mail(){
        String username = sharedpreferences.getString(Username,"");
        String groupname = sharedpreferences.getString(Groupname,"");
        Intent intentEmail = new Intent(Intent.ACTION_SEND);
        intentEmail.setType("message/rfc822");
        intentEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{"ekta@zeslab.com"});
        intentEmail.putExtra(Intent.EXTRA_CC, new String[]{"bhaskar@zeslab.com"});
        intentEmail.putExtra(Intent.EXTRA_SUBJECT, "Generate yearly report");
        intentEmail.putExtra(Intent.EXTRA_TEXT, "Dear sir, \nMy username is" +
                " "+username+" and my groupname is "+groupname+" and transactionId" +
                " is"+mTransactionId+" and amount is"+mAmount+".Kindly send me" +
                " my yearly attendance report.\n\nThank You.");
        startActivity(Intent.createChooser(intentEmail, "Send Email to get Yearly report"));
    }
}
