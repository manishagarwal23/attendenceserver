var mongoose = require('mongoose')

/////////********  mongoose schema
var schema = mongoose.Schema({
User_Id:{
  type:String
},
total_work_days:{
  type:Number,
  default:0
},
current_Date:{
type: String
},
current_Year:{
	type:String
},
current_Month:{
 	type:String
 }
});

///////////////   creating model
var WorkDays = mongoose.model('WorkDays', schema);

module.exports={WorkDays}
