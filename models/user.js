var mongoose = require('mongoose')

/////////********  mongoose model
var Users = mongoose.model('Users',{
  name:{
    type: String,
    minlength: 2,
    trim: true
  },
  age:{
    type:String
  },
  mobile:{
    type: String
  },
  username:{
    type: String,
    unique:true,
    minlength:3,
    trim:true
  },
  email:{
    type: String,
    default:"abc@gmail.com"
  },
  password:{
    type: String
  },
  fb_ID:{
    type: String
  },
  fb_Token:{
    type: String
  },
  officeLocation:{
    type:String
  },
  officeLat:{
    type:Number
  },
  officeLong:{
    type:Number
  },
  subordinateId:{
    type:Array
  },
  managerId:{
    type:String
  },
  roleId:{
    type:Number,
    default:1
  },
  companayName:{
    type:String
  },
  companayId:{
    type:String
  },
  lastLoginTime:{
    type: Date,
    default: Date.now 
  }
});

module.exports={Users}
