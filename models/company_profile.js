var mongoose = require('mongoose')

/////////********  mongoose model
var Company = mongoose.model('Company',{
  Cname:{
    type: String,
    minlength: 2,
    trim: true
  },
  Mvisible:{
    type:Boolean
  }
});

module.exports={Company}
