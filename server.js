const express = require('express')
var bodyParser = require('body-parser')
var morgan = require('morgan')
var dateFormat = require('dateformat');
// var sync = require('sync');
// var promise =  require('promise');
var geolib = require('geolib');
var {mongoose}= require('./db/mongoose')


var app = express()

 ////////// middelware morgan
 app.use(morgan('dev'))

 app.use(express.static(__dirname + '/public'))

 var {Users} = require('./models/user')
 var {Attendence} = require('./models/user_attendence')
 var {WorkDays} = require('./models/user_work_days')
 var {savingDailyAttendenceFunction} = require('./dbutil')
 var {saveDayCountFunction} = require('./dbutil')
 var {makeUserEnterIntoOffice} = require('./userEnersInOffice')
 var {makeUserRejectFromOffice} = require('./userEnersInOffice')
 var {increasedays} = require('./dbutil')
 var {signupFunction} = require('./dbutil')
 var {Company} = require('./models/company_profile')

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })
// alternate use of body-parser
app.use(bodyParser.json());

///////   function for getting date and time
// function getMyDate(){

  // var in_isoTime = dateFormat(now,"isoTime")
  // console.log(in_isoTime);
  // var in_Date = dateFormat(now,"shortDate")
  // console.log(in_Date);
  // var out_isoTime = dateFormat(now, "isoTime")
  // console.log(out_isoTime);
  // var out_Date = dateFormat(now, "shortDate")
  // console.log(out_Date);

// }

// var userAgent = System.getProperty( "http.agent" );
// console.log('hey @@@@@ are you :'+ userAgent);

function callIfValidLoggedIn(req, res, callback)
{
  // check if user is logged in 

    callback(req,res);
}
///// function for sleep
function sleep(){
  var ms =60;
  var date = new Date();

  console.log("GOING sleep for 1 min"+ date.getTime());
  var sleep = require('system-sleep');
  sleep(ms*1000);
  console.log("sleep for 1 min DONE"+ date.getTime());
}
/*          login and signupp functions    */
// ///////////////   function for signup
// function signup(req, res) {
//   var user = new Users({
//     username: req.body.username,
//     mobile: req.body.mobile,
//     email: req.body.email,
//     password: req.body.password,
//     managerId: req.body.managerId,
//     roleId: req.body.roleId,
//     companyName: req.body.companyName
//   });

//   user.save().then((doc)=>{
//     res.send(doc)
//     console.log(doc)
//   },(e)=>{
//     res.status(400).send(e);
//   });
//   console.log(res.body);
// }
////////////////   function for restrict user to signup if allready exist
function SignUp(req, res){
    var user = new Users({
    username: req.body.username,
    name: req.body.name,
    mobile: req.body.mobile,
    email: req.body.email,
    password: req.body.password,
    managerId: req.body.managerId,
    roleId: req.body.roleId,
    companyName: req.body.companyName
  });
    Users.findOne({username:req.body.username},function(err,userfound){
      if(err){
        console.log(err)
        return res.status(500).send('error while signup'+err)
      }
      if(!userfound){
        console.log("============================");
        console.log('this is unique username :' + req.body.username)
        signupFunction(user,res);
      }
      if(userfound){
        res.json({result:false,data:"user already exist"});
      }
    })
}

////////// function for login
function login(req,res){
  var mUsername = req.body.username;
  var mPassword = req.body.password;
  Users.findOne({username:mUsername,password:mPassword}, function(err, user){
    if(err){
      console.log(err);
      return res.json({result:false,msg:err});
    }
    if(!user){
      return res.json({result:false,msg:"user not found"});
    }
    // modigyLastLogin(req,res,mUsername)
    return res.json({result:true,Data:user});
  })
}

////////////////  function for facebook login
function facebook(req, res){
  var email = req.body.email;
  var users = new Users({
    username: req.body.username,
    name: req.body.name,
    mobile: req.body.mobile,
    email: req.body.email,
    password: req.body.password
   
  });
  Users.findOne({email:email}, function(err, user){
    if(err){
      console.log(err);
      return res.json({result:false,Data:err});
    }
    if(!user){
     console.log("new user from facebook signup");
     console.log(req.body.email);
     return signupFunction(users,res);
   }
   console.log("returing facebook user logged in");
   console.log(user);
   return res.json({result:true,Data:user});

 })
}

///////////////////// function for show user by managerid
function userUnderManager(req, res){
  Users.findOne({_id:req.body._id}, function(err,todos){
    if(err){
      console.log('error while finding userid'+err);
      return res.json({result:false,msg:err});
    }
    else if(!todos){
      console.log('user not found');
      return res.json({result:false,msg:'user not found'})
    }
    else if(todos){
      console.log('user found');
      return res.json({result:true,Data:todos.subordinateId})
    }
  })
  // Users.find({username:req.body.username}).then(todos =>{
  //   res.json({Data: todos})
  //   next()
  // })
  // .catch(e =>{
  //   res.json({msg:e});
  // })
}

///////////////////////   show my manager 
function showMymanagerInternal(req,res)
{
    Users.find({roleId:req.body.roleId}).then(todos =>{
    res.json({result:true,Data:todos})
  })
  .catch(e=>{
    res.json({result:false,msg:e});
  })

}
function showMyManager(req, res){
  Users.find({roleId:req.body.roleId}).then(todos =>{
    res.json({result:true,Data:todos})
  })
  .catch(e=>{
    res.json({result:false,msg:e});
  })
  // callIfLoggedIn(req,res,showMymanagerInternal);
 }

///////////////////////    set my manager  
function setMyManager(req,res){
Users.findOneAndUpdate({_id:req.body.User_Id},{$set:{managerId:req.body.managerId}},{new: true}).then(todos =>{
    res.json({result:true,Data:todos})
  })
  .catch(e=>{
    res.json({result:false,msg:e})
  })
}
//////////////////// set my subordinate
// function setMySubordinate(req, res){

// checkCompany(req,res,setSubordinate);
  
// }
// /////////////////// callback function that will check same company and allow to add or not
// function checkCompany(req,res,setSubordinate){
  


// }

// ////////////////  check user exist or not 
// function checkUser(req,res){
//   Users.findOne({username:username},function(err,userFound){
//     if(err){
//       console.log("error while finding username and error is :"+ err)
//       return res.json({result:false,msg:err})
//     }
//     if(!userFound){
//       console.log("user not found");
//       return res.json({result:false,msg:"user not found"})
//     }
//     console.log('user found')
//   })
// }

/////////////// set subordinate if all test pass
function setMySubordinate(req,res){

Users.findOne({username:req.body.subordinateId}, function(err,subuserfound){
    if(err){
      console.log("error while finding subordinate and error is :"+ err)
      return res.json({result:false,msg:err})
    }
    if(!subuserfound){
      console.log("user that you are searching to add in my subordinate is not found");
      return res.json({result:false,msg:"subordinate user not found"})
    }
  console.log("user found now you can add in your subordinate")
  var query = {_id:req.body.User_Id},
      update = {
          $set:{roleId:"2"},
          $push:{subordinateId:req.body.subordinateId}
               },
    options = {upsert: true};
    Users.findOneAndUpdate(query, update, options, function(err, todos){
      if(err){
        return  res.json({result:false,msg:e});
      }
      else if(!todos){
       return res.json({result:false,msg:"user not found"});
      }
      else if(todos){

        return res.json({result:true,Data:todos});
      }
    })
})
}
//  Users.update({_id:req.body.User_Id},{$push:{subordinateId:req.body.subordinateId}}).then(todos =>{
//      res.json({result:true,Data:todos})
//   })
//   .catch(e=>{
//     res.json({result:false,msg:e})
//   })
// })

////////////////////   setting user's office location function
function setOfficeLoc(req, res){
  var userid = req.body.User_Id;
  var officelocation = req.body.officelocation;
  Users.findOneAndUpdate({_id:userid}, {$set:{officeLat:req.body.officeLat,officeLong:req.body.officeLong}}, {new: true},
   function(err, location){
    if(err){
      console.log(err);
      return res.status(500).send(err);
    }
    if(!location){
     console.log(req.body.User_Id);
     return res.send(404).send(userid);
   }
   console.log(location);
   return res.status(200).send(location);

 })
}

//////////////  function for checking the distance between them
// 
function checkDistancefromOffice(req,isnearstatus,res,callUserNear, callUserFaR){
  console.log("calling function of distance for ====>"+ req);
  console.log("calling function of distance for user id "+ req.body.User_Id);
  Users.findOne({_id:req.body.User_Id},  function(err, location){
    if(err){
      console.log("some error we will return")
      console.log(err);
      return res.status(500).send(err);
    }
    if(!location){
      console.log("some error we will return 22222")
      console.log(req.body.User_Id);
      return res.send(404).send();
    }
    console.log(location.officeLat);
    console.log(location.officeLong);

    console.log(req.body.officeLat);
    console.log(req.body.officeLong);

    var dfo = geolib.getDistance(
      {latitude: req.body.officeLat, longitude: req.body.officeLong},
      {latitude: location.officeLat, longitude: location.officeLong}
      ); 
    console.log("in the function of checking distance called");
    if(dfo<=300){
     isnearstatus = true;
    
     console.log("isnearstatus changed to true")

     
     callUserNear(req,isnearstatus,res);
   }else{
    isnearstatus = false;
        callUserFaR(req,isnearstatus,res,dfo);
        console.log("isnearstatus changed to false")
  }
  console.log("distance from office "+ dfo);

      // return res.status(200).send(location);
    })
  // return ;
}


//////////   function for showing users table data
function sendAllUserData (req, res, next){
  Users.find().then(tasks => {
    res.send(200, tasks)
    next()
  })
  .catch(err => {
    res.send(500, err)
  })
};

///////////////////////////   function user/month
function UserMonthData(req,res){
  var ui = req.body.User_Id;
  var month = req.body.mMonth;
  Attendence.find({User_Id:ui,mMonth:month}, function(err, todos){
    if(err) {
            // return console.log(err);
            return res.status(500).json({message: err.message});
          }

        res.json({Data: todos}); //2nd todos = the array import above
      });
}

///////////////////////////   function user/totalworkday
function UserTotalWorkDay(req,res){
  var ui = req.body.User_Id;
  WorkDays.find({User_Id:ui}, function(err, todos){
    if(err) {
            // return console.log(err);
            return res.status(500).json({message: err.message});
          }

        res.json({Data: todos}); //2nd todos = the array import above
      });
}

//////////////  function for calling how many days he work in month
function howManyDaysInMonth(req,res){
  Attendence.find({username:req.body.username,mMonth:req.body.mMonth,mYear:req.body.mYear}).then((tasks) =>{
    console.log('getting how many days he worked in months'+ tasks.length);
      console.log('getting how many days he worked in months'+ req.body.username);
    res.json({result:true,Data:tasks.length})
  })
  .catch(err =>{
    console.log('error while getting userid,month'+err)
    res.json({result:false,msg:err})
  })
}
//////////////////// function for calling how many days he work in year
function howManyDaysInYear(req,res){
  Attendence.find({username:req.body.username,mYear:req.body.mYear}).then(tasks =>{
    console.log('getting how many days he worked in year'+tasks.length);
    res.json({result:true,Data:tasks.length})
  })
  .catch(err =>{
    console.log('error while getting userid,month'+err)
      res.json({result:false,msg:err})
  })
}
//////////////   function for showing usersWorkingHours
function usersWorkingHours (req, res, next){
  Attendence.find().then(tasks => {
    res.send(200, tasks)
    next()
  })
  .catch(err=>{
    res.send(500,err)
  })
}

/////// function for getting specific user attendence
function specific_user_attendence(req,res){
  var userid= req.body.User_Id;
  console.log(userid);
  Attendence.find({User_Id:userid}, function(err, todos){
    if(err) {
              // return console.log(err);
              return res.status(500).json({message: err.message});
            }

          res.json({Data: todos}); //2nd todos = the array import above
        });
}

//// POST method user_data for user collenctions
app.post('/userdata', function (req, res) {
  var user = new Users({
    name: req.body.name,
    mobile: req.body.mobile,
    email: req.body.email,
    password: req.body.password,
    fb_ID: req.body.fb_ID,
    fb_Token: req.body.fb_token
  });
  user.save().then((doc)=>{
    res.send(doc)
  },(e)=>{
    req.status(400).send(e)
  });
  console.log(req.body);
});


////////  POST method for  users attendence
app.post('/attendence', function (req, res) {

  var now = new Date();

  var attendence = new Attendence({
    User_Id: req.body.User_Id,
    mYear: dateFormat(now, "yyyy"),
    mMonth: dateFormat(now, "mmmm"),
    Location_on_InTime: req.body.Location_InTime,
    Location_on_OutTime: req.body.Location_on_OutTime,
    action_to_perform: req.body.action_to_perform
  });

  var workdays = new WorkDays({
    User_Id: req.body.User_Id,
    total_work_days: 1,
    current_Month:dateFormat(now, "mmmm"),
    current_Date: dateFormat(now, "dd/mm/yyyy")
  })


  if(req.body.action_to_perform=='InTime'){
    var isnearstatus = false;

    checkDistancefromOffice(req,isnearstatus,res,makeUserEnterIntoOffice, makeUserRejectFromOffice);

}

else if(req.body.action_to_perform=='OutTime'){
  var id = req.body.id;
  var now2 = new Date();
  var queryForAttendance = Attendence.findOne({User_Id:req.body.User_Id})
                                   .where('mDate').equals(dateFormat(now2,"dd"))
                                   .where('mMonth').equals(dateFormat(now2,"mmmm"))
                                   .where('mYear').equals(dateFormat(now2,"yyyy"));

  queryForAttendance.exec(function (err, attendence2){
        if(err){
          console.log('---------  !!!!!!! err queryForAttendance'+ err)
        }
        else if(!attendence2){
          console.log('attendence2 not found ')
          // savingDailyAttendenceFunction(attendence,res);
          // have to create new one
        }
        else if(attendence2){
     var vUser_Id   =   attendence2.User_Id;
     var vYear      =   attendence2.mYear;
     var vMonth     =   attendence2.mMonth;
     var vDate      =   attendence2.in_Date;
     var vTime      =   attendence2.in_Time;
     var inLocation =   attendence2.Location_on_InTime;
     attendence.User_Id            = vUser_Id;
     attendence.mYear              = vYear;
     attendence.mMonth             = vMonth;
     attendence.mDate              = attendence2.mDate;
     attendence.in_Date            = vDate;
     attendence.in_Time            = vTime;
     console.log("intime:"+vTime);
     attendence.Location_on_InTime = inLocation;
     attendence.out_Time = dateFormat(now2, "isoTime");
     attendence.out_Date = dateFormat(now2,"dd/mm/yyyy");
     var intime = attendence.in_Time;
     var outtime = attendence.out_Time;
     console.log('==============================')

     console.log('intime'+ intime)

     console.log('outtime'+ outtime)
     console.log('==============================')
     var inhours = Number(intime.substring(0,2)) + Number(intime.substring(3,5))/60 + Number(intime.substring(6,8))/3600;
     console.log(inhours);
     var outhours = Number(outtime.substring(0,2)) + Number(outtime.substring(3,5))/60 + Number(outtime.substring(6,8))/3600;
     console.log(outhours);

     console.log("we are going to add total time: " + attendence2.total_Time);
     attendence.total_Time = attendence2.total_Time + (outhours - inhours) ;

         ////  delete
         Attendence.remove({ _id: id }, function(err) {
           savingDailyAttendenceFunction(attendence,res)
         });
       }
     })
}
  // console.log(req.body);
});


// POST /login gets urlencoded bodies
// app.post('/SignUp', urlencodedParser, signup )

/////////////// set my subordinate
app.post('/setMySubordinate', setMySubordinate);

///////////////  signup via app
app.post('/signupIfNew',SignUp);

//////////////////////   Login method
app.post('/login',login);

////////////////////////    facebook login
app.post('/login/facebook',facebook);

//////////////////////////  post for saving user's office locations
app.post('/setofficeLoc',setOfficeLoc);

/////////////////  find user under manager
app.post('/users/manager',userUnderManager);
//////// find method for finding the a perticular user attendence
app.post('/SU_attendence', specific_user_attendence);

//////////////////////////   find the all working days of user
app.post('/user/totalworkdays', UserTotalWorkDay);

///////////////////// user/month
app.post('/user/month',UserMonthData);

/////////////  route for calling how many days he work in month
app.post('/month/worked', howManyDaysInMonth);

/////////////  route for calling how many days he work in year
app.post('/year/worked', howManyDaysInYear);

///////////   test for update methods
app.post('/update1',function(req,res){
  Users.findOneAndUpdate({_id:req.body.User_Id}, {$set:{name:"bhaskar"}}, {new: true}, function(err, doc){
    if(err){
      console.log("Something wrong when updating data!");
    }

    console.log(doc);
    return res.status(200).send(doc)
  });
})

//////////////////   get methods
app.get('/users', sendAllUserData);

app.get('/usersWorkingHours',usersWorkingHours);

// app.get('/', (req, res) =>{
//   res.writeHead(302, {
//     'Location': '/form.html'
//   //add other headers here...
// });
//   res.end();
// });

app.listen(3000, ()=>
{
  console.log('server is up and running on port 3000');
 });

